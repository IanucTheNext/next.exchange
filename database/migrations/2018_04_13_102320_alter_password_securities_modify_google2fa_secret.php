<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPasswordSecuritiesModifyGoogle2faSecret extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('password_securities', function ($table) {
            /** @var Illuminate\Database\Schema\Blueprint $table */
            $table->string('google2fa_secret', 220)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('password_securities', function ($table) {
            /** @var Illuminate\Database\Schema\Blueprint $table */
            $table->string('google2fa_secret')->nullable()->change();
        });
    }
}

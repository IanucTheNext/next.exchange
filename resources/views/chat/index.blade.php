@extends('_layouts.main')

@section('content')
    @include('_partials.flashalert')
    <?php
    define('REFRESH_SECONDS', 30);
    ?>

    <div id="chat">
        <div class="container ptb40">
            <div class="row">
                <div class="col-md-8 col-md-offet-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <h1>Chatroom</h1>
                            <span class="badge pull-right">@{{ usersInRoom.length }}</span>
                        </div>
                        <chat-log :messages="messages"></chat-log>
                        <chat-composer v-on:messagesent="addMessage"></chat-composer>
                        </div>
                    </div>
            </div>
        </div>
    </div>


@endsection

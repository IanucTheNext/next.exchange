<footer id="footer">
    <div class="main">
        <div class="logo-lang">
            <div>
                <div class="logo">
                    <img src="{{ asset('/img/next-exchange-logo-white.png') }}" alt="" width="200">
                </div>
            </div>
            <div>
                <div class="lang d-none d-sm-block">
                    <div class="lang-select">
                        <div class="lang-list">
                            <a id="lang_btn" data-lang="nl">Dutch</a>
                        </div>
                        <span class="current-lang">English</span>
                    </div>
                </div>

            </div>
        </div>
        <div class="links-group">
            <div class="info">
                <div class="open-btn">+</div>
                <a class="title show-item" data-lang-id="footer_social">Terms of Service</a>
                <a href="/terms" data-lang-id="mobi_footer_fees">Terms & Conditions</a>
                <a href="/AMLKYC" data-lang-id="mobi_footer_fees">AML/KYC Policy</a>
                <a href="/privacy" data-lang-id="mobi_footer_pp">Privacy Disclaimer</a>


                <a href="mailto:info@next.exchange">info@next.exchange</a>
            </div>
            <div class="social">
                <div class="open-btn">+</div>
                <a class="title show-item" data-lang-id="footer_social">Social</a>
                <a href="https://www.facebook.com/nextexchanger/"><i class="fontello-facebook"></i> Facebook</a>
                <a href="https://twitter.com/nextexchange"><i class="fontello-twitter"></i> Twitter</a>
                <a href="https://www.linkedin.com/company/nextexchange/"><i class="fontello-linkedin"></i> LinkedIn</a>
            </div>
        </div>
        <div class="col-md-12 d-sm-flex align-items-end">
            <p class="copyrights mt-5 mb-2 mb-sm-0">
                © 2017-2018 - NEXT.exchange is a trademark of OpenTrader N.V. (i.o.)
            </p>
        </div>
    </div>
</footer>

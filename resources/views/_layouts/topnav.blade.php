
<?php
// Set attributes for homepage
if(Request::is('/')) {
    $logo = asset('/img/next-exchange-logo-white.png');
    $class_header = 'transparent';
}
else {
    $logo = asset('/img/next-exchange-logo-orig.png');
    $class_header = 'default';
}
?>

<header class="<?= $class_header ?>" id="index_header">
    <nav class=" container" >
        <a class="logo" href="/">
            <img src="<?= $logo ?>" style="padding-top: 6px;" alt="Next Exchange Stock Market Logo">
        </a>
        <div class="menu-icon"><span></span></div>
        <ul class="menu">
            @if(auth()->user())
                <li>
                    <a href="/exchange/beta">Exchange</a>
                </li>
            @endif
            <li>
                <a href="/social">Community</a>
            </li>

            <li>
                <a href="/marketcap">MarketCap</a>
            </li>
            <li>
                <a href="https://nextexchange.featureupvote.com">Suggestions</a>
            </li>

            <li>
                <a href="https://medium.com/nextexchange">Blog</a>
            </li>
            <li>
                <a href="{{ url('contact') }}">Contact</a>
            </li>

            @if(!Auth::check())
                <li>
                    <a href="{{ url('login') }}">Login</a>
                </li>

                @if(!Request::is('register'))
                    <li >
                        <a href="{{ url('register') }}">Register</a>
                    </li>
                @endif

            @else
                <li class="dropdown navbar-user">
                    <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">

                        <span class="avatar avatar-home"><img src="{{ Gravatar::get(Auth::user()->email) }}" class="img-responsive" width="32px" alt="" /></span>
                        <span class="hidden-xs">{{ Auth::user()->name }}</span> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu" style="z-index: auto;">
                        <li><a href="/home">Members area</a></li>
                        <li><a href="/profile">Edit Profile</a></li>
                        <li><a href="/id-proof">Upload Id Proof</a></li>
                        <li class="divider"></li>
                        <li><a href="/logout">Log Out</a></li>
                    </ul>
                </li>

            @endif
        </ul>
    </nav>



</header>
<!--/Navigation-->

<header class="<?= $class_header ?>" id="index_header_scroll">
    <nav class=" container" >
        <a class="logo" href="/">
            <img src="<?= $logo ?>" alt="Next Exchange Stock Market Logo">
        </a>
        <div class="menu-icon"><span></span></div>
        <ul class="menu">
            <li>
                <a href="/community">Community</a>
            </li>

            <li>
                <a href="https://nextexchange.featureupvote.com">Suggestions</a>
            </li>

            <li>
                <a href="https://medium.com/nextexchange">Blog</a>
            </li>
            <li>
                <a href="{{ url('contact') }}">Contact</a>
            </li>

            @if(!Auth::check())
                <li>
                    <a href="{{ url('login') }}">Login</a>
                </li>

                @if(!Request::is('register'))
                    <li >
                        <a href="{{ url('register') }}">Register</a>
                    </li>
                @endif

            @else
                <li class="dropdown navbar-user">
                    <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">

                        <span class="avatar avatar-home"><img src="{{ Gravatar::get(Auth::user()->email) }}" class="img-responsive" width="32px" alt="" /></span>
                        <span class="hidden-xs">{{ Auth::user()->name }}</span> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu" style="z-index: auto;">
                        <li><a href="/home">Members area</a></li>
                        <li><a href="/profile">Edit Profile</a></li>

                        <li class="divider"></li>
                        <li><a href="/logout">Log Out</a></li>
                    </ul>
                </li>

            @endif
        </ul>
    </nav>
</header>

@extends('_layouts.main')

@section('content')

    @include('_layouts.topnav')

    @include('_partials.status-panel')

    <section class="text-center">
        <div class="container">
            <div class="row ptb40">
                <div class="col-sm-12">
                    <h1 class="page-heading">Community</h1>
                    <br>
                    <p class="lead">
                        NEXT is a large community of experienced investors, companies, and traders. So, start exploring!
                    </p><br>
                    <a class="btn btn--primary type--uppercase buy-token" href="/register">
                                    <span class="btn__text">
                                       Join Our Movement
                                    </span>
                    </a>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-block">
                        <h3>Current Members</h3> <br>
                    </div>
                </div>
                @foreach($users as $user)
                    <div class="col-sm-4">
                        <div class="feature feature-1 boxed boxed--border">
                            <span class="badge badge-default float-right">{{ $user->title}}</span>
                            <img class="card__avatar" src="{{ Gravatar::get($user->email) }}" alt="{{ $user->name }}">
                           <div class="user-details">
                            <p class="f16 text-black"><?= substr($user->name,0, -3).'***'; ?></p>
                            <p>
                                {{ $user->about_me}}
                            </p>
                            <!-- <a href="/profile"> -->
                            <a href="{{ url('profile', $user->id) }}">
                                View Profile
                            </a>
                            </div>
                        </div>
                        <!--end feature-->
                    </div>
                @endforeach
            </div>
            <nav aria-label="Page navigation example">
            {{ $users->links() }}
            </nav>
        </div>
        <!--end of row-->
        </div>
        <!--end of container-->
    </section>


@endsection

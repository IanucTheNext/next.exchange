@extends('_layouts.main')

@section('content')

    @include('_layouts.topnav')

    @include('_partials.status-panel')

<!--
    <div class="container">
    <nav class="breadcrumb">
        <a class="breadcrumb-item" href="/">Home</a>
        <a class="breadcrumb-item" href="/contact">Contact</a>
    </nav>
    </div>
-->



    <section id="apply">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 offset-lg-2 col-md-12 offset-md-1 text-center mt-4 preamble">
                    <h3 class="title mt-0 mb-3 page-heading">Contact us</h3>

                    <p class="lead mb-5"> <span>Leave us a message, give us a call, or stop by our office anytime.<br/> We try to answer all enquiries and questions within 3 working days. <br><br>For urgent questions, please use the 'LEAVE A MESSAGE' box, which you can find on the bottom right of the screen.
                           </span><br><br></p>
                </div>
            </div>



            <section class="switchable ">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <img src="/img/NEXT_singel_250.jpg" class='img-fluid' alt="Office NEXT - the Netherlands">
                            <p class="">

                                <br>
                                <b>Main office - AMSTERDAM</b>

                                <br><i class="fa fa-address-book" aria-hidden="true"></i> Singel 250<br> 1016 AB Amsterdam <br>the Netherlands
                                <br /><i class="fa fa-phone-square" aria-hidden="true"></i> +31 85 8886240
                                <br> <i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:info@next.exchange">info@next.exchange</a>
                                <br /><i class="fa fa-skype" aria-hidden="true"></i> next.exchange
                            </p>


                        </div>
                        <style>
                            .form-modern { padding: 40px; }
                            .form-modern .btn { padding-left: 10px !important; }
                        </style>
                        <div class="col-sm-8 col-xs-12 ">
                            <div class="row card ">



                                @if (session('message'))
                                    <div class="alert alert-info">{{ session('message') }}</div>
                                @endif

                                <ul>
                                    @if(isset($message))
                                        <li>{{ $message }}</li>
                                    @endif
                                </ul>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                {!! Form::open(array('url' => '/contact', 'class' => 'form form-modern ')) !!}
                                <div class="form-group">
                                    {!! Form::label('Your Name') !!}
                                    {!! Form::text('name', null,
                                        array('required',
                                              'class'=>'form-control',
                                              'placeholder'=>'Your name')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('E-mail Address') !!}
                                    {!! Form::text('email', null,
                                        array('required',
                                              'class'=>'form-control',
                                              'placeholder'=>'Your e-mail address')) !!}

                                       @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong style="color:red;">{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                            
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Message') !!}
                                    {!! Form::textarea('message', null,
                                        array('required',
                                              'class'=>'form-control',
                                              'style'=>'height:150px',
                                              'placeholder'=>'Your message')) !!}
                                </div>
                                    @if(config('settings.reCaptchStatus'))
                                        <div class="form-group">
                                            <div class="col-sm-6 col-sm-offset-4">
                                                <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                            <strong style="color:red;">{{ $errors->first('captcha') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                {{--<div class="form-group" >
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <label>
                                                {!! captcha_img() !!}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }}">
                                        <label class="col-md-4 control-label">Captcha</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="captcha" autocomplete="off">
                                            @if ($errors->has('captcha'))
                                                <span class="help-block">
                                                    <strong style="color:red;">{{ $errors->first('captcha') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>--}}
                                <br>
                                <div class="form-group">
                                    {!! Form::submit('Send Enquiry',
                                      array('class'=>'btn btn-primary')) !!}
                                </div>

                                {!! Form::close() !!}
                            </div>
                            <!--end of row-->
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>




        </div>
    </section>
@section('scripts')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@stop
@extends('_layouts.main')

@section('content')

    @include('_layouts.topnav')

 <!-- <div class="banner banner-mini bg-blue">
        <span data-lang-id="about_pageTitle">Login</span>
    </div>
-->

 <style>
     .login { background-color: #fff !important; }
     .login p { padding-top: 30px; padding-bottom: 20px; }
     .login h3 { padding-bottom: 20px; }
     .login button { margin-top: 20px; margin-bottom: 20px; }
 </style>

    <section class="login">
        <div class="container">
            <div class="col-md-12 login-box register-box">
  <h3 style="margin-left: 16px;" class="text-center">Login</h3>
                <a class="flat-butt-sm btn-social btn-facebook" href="/social/redirect/facebook">
                    <i class="fa fa-facebook fa-fw"></i> Login with Facebook
                </a>


                  <p style="margin-left: 16px; margin-bottom:20px" class="text-center">Or login with your NEXT account</p>

              <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                    @if ($errors->has('2fatoken'))
                      <div class="col-md-12 has-error">
                          <span class="help-block">
                                        <strong>{{ $errors->first('2fatoken') }}</strong>
                                    </span>
                      </div>
                    @endif

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-12 control-label">Password</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4 text-center">
                                <button type="submit" class="btn btn-primary signup-btn" style="cursor: pointer">
                                    Login
                                </button>


                                <a class="btn-link block forgot-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </section>
@endsection



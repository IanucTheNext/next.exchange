@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/authy-form-helpers/2.3/flags.authy.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/authy-form-helpers/2.3/form.authy.css" />
@endsection

<div class="col-12">
    <form method="POST" action="{{url('auth/two-factor')}}">
        {{csrf_field()}}

        <div class="row">
        <div class="form-group">
            <label for="exampleSelect1">Country:</label>
            <select id="authy-countries" name="country-code"></select>
        </div>

        <div class="form-group">
            <label for="exampleSelect1">Phone number:</label>
            <input id="authy-cellphone" type="text" value="" name="authy-cellphone" />
        </div>

        <div class="form-check">
            <label class="form-check-label">
                <input type="checkbox" name="send_sms" />
                Send two-factor token via SMS
            </label>
        </div>
        </div>


        <div class="row">
            <div class="col-xs-9 col-xs-offset-3">
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
        </div>

        <div class="col-xs-3"><br><small>Powered by <a href="https://www.authy.com" target="_blank">Authy.com</a> - <a href="https://play.google.com/store/apps/details?id=com.authy.authy">Android (APP)</a> - <a href="https://itunes.apple.com/us/app/authy/id494168017">IOS (APP)</a></small></div>


    </form>
</div>

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/authy-form-helpers/2.3/form.authy.js"></script>
@endsection
@extends('_layouts.main')

@section('content')
    @include('_partials.flashalert')
    <?php
    define('REFRESH_SECONDS', 30);
    ?>

    @include('_partials.exchange-nav')

    <section id="members" class="ptb40 bg--secondary">
        <div class="container">
            <img class="center" src="https://files.coinmarketcap.com/static/img/coins/128x128/safe-exchange-coin.png" height="128"><br>
            <h1 class="center">SAFEX</h1><br>
            <div id="exchange_next"></div>


            <div id="error" class="center"></div>
            <br>

        </div>
    </section>


    <script>
        $( "#exchange_next" ).load( "http://localhost:8002", function( response, status, xhr ) {
            if ( status == "error" ) {
                var msg = "Sorry but there was an error: ";
                //$( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
                $("#error").html('SAFEX integration postponed until further notice.')
            }
        });
    </script>

@endsection
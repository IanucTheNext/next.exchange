@extends('_layouts.main')

@section('content')
    @include('_partials.flashalert')
    <?php
    define('REFRESH_SECONDS', 30);
    ?>

    @include('_partials.exchange-nav')

    <section id="members" class="ptb40 bg--secondary">
        <div class="container">
            <img class="center" src="/img/coin/ORIG/NEXT.png" height="128"><br>
            <h1 class="center">NEXT TRADING TOOL (BETA)</h1><br>
            
            <div class="app-odrs">
                <main class="exchanger-app text-center" id="exchanger_err" style="display: none">
                    <h3>Something is wrong.<br/> Unable to connect with Metamask or Smart Contract</h3>
                </main>
                <main class="exchanger-app" id="exchanger_app" style="display: none">
                    <div class="app-inner">
                        
                        <section class="orders-container">
                            <div class="orders-inner">
                                <div class="clearfix">
                                    <h5 class="float-left">
                                        <span class="text-uppercase" id="walletAddress"></span>
                                    </h5>
                                    <h5 class="float-right">
                                        <span class="text-uppercase" id="tokenBalance"></span> | <span class="text-uppercase" id="etherBalance"></span>
                                    </h5>
                                </div>  
                                <hr>
                                <div class="btn-orders">
                                    <a href="#" id="lnkMakeOrder" class="btn btn-primary waves-effect">Make Order</a>
                                    <a href="#" target="_blank" class="btn btn-default waves-effect lnkTransactions">See All Transaction</a>                                    
                                    <!-- <a href="#" id="lnkResetAllowance" class="btn btn-danger waves-effect">Reset Allowance <span class="tokenAllowance"></span></a> -->
                                </div>
                                <hr>
                                <div class="text-left">
                                    <h2 class="m-b-15">Important Instructions</h2>
                                    <ul class="list">
                                        <li>Please note that it will take a while to process transaction</li>
                                        <li>You can <a href="#" target="_blank" class="lnkTransactions">click here</a> to see all of your transactions</li>
                                        <li>When order is placed, it requires certain amount of time to appear that order on blockchain</li>
                                        <li>If you are selling your tokens, amount will only get deducted when someone takes your order</li>
                                        <li>Order can only be deleted by owner of order, When it is deleted it will take sometime to reflect on blockchain</li>
                                        <li>When order is taken, ethers are directly sent to seller of tokens and it will take sometime to reflect order status and your balance</li>
                                        <li>All orders and data is stored and secured on blockchain network. Transaction is signed using metamask and we do not hold your private key</li>
                                    </ul>
                                </div>
                                <div class="form-make-order">
                                    <div aria-hidden="true" aria-labelledby="myModalLabel" class="modal fade" id="modalMakeOrder" role="dialog" tabindex="-1">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header bg-primary white-text">
                                                    <h4 class="title">
                                                        <i class="fas fa-cart-plus"></i> Make Order
                                                    </h4>
                                                    <button aria-label="Close" class="close waves-effect waves-light text-white" data-dismiss="modal" type="button">
                                                        <span aria-hidden="true">
                                                            <span class="fas fa-times-circle"></span>
                                                        </span>
                                                    </button>
                                                </div>
                                                <div class="modal-body mb-0">
                                                    <form id="frmMakeOrder" name="frmMakeOrder" action="#">
                                                        <div class="md-form form-sm">
                                                            <input type="text" class="form-control disabled text-uppercase" id="makeWalletAddress" placeholder="-" tabindex="-1" readonly/>
                                                            <label for="makeWalletAddress">Wallet Address</label>
                                                        </div>
                                                        <div class="md-form form-sm">
                                                            <input type="text" class="form-control disabled text-uppercase" id="makeTokenAddress" placeholder="-" tabindex="-1" readonly/>
                                                            <label for="makeTokenAddress">Token Address</label>
                                                        </div>
                                                        <div class="md-form form-sm">
                                                            <input type="text" class="form-control disabled" id="makeTokenName" placeholder="-" tabindex="-1" readonly/>
                                                            <label for="makeTokenName">Token Name</label>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="text" class="form-control disabled" id="makeTokenSymbol" placeholder="-" tabindex="-1" readonly/>
                                                                    <label for="makeTokenSymbol">Token Symbol</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="text" class="form-control disabled" id="makeTokenDecimal" placeholder="-" tabindex="-1" readonly/>
                                                                    <label for="makeTokenDecimals">Token Decimals</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="text" class="form-control disabled" id="makeEtherBalance" placeholder="-" tabindex="-1" readonly/>
                                                                    <label for="makeEtherBalance">ETH Balance</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="text" class="form-control disabled" id="makeTokenBalance" value="-" tabindex="-1" readonly/>
                                                                    <label for="makeTokenBalance">Token Balance</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="number" class="form-control" id="makeTokenAmount" min="1" step="any" />
                                                                    <label for="makeTokenAmount">Tokens to Sell</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="number" class="form-control" id="makeEtherAmount" step="any" />
                                                                    <label for="makeEtherAmount">How many Ether</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="number" class="form-control disabled" id="makeTokenRateEther" value="-" tabindex="-1" readonly/>
                                                                    <label for="makeTokenRateEther">Token Price in Ether</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="number" class="form-control disabled" id="makeTokenRateUsd" value="-" tabindex="-1" readonly/>
                                                                    <label for="makeTokenRateUsd">Token Price in USD</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="md-form-btn text-right">
                                                            <button class="btn btn-primary">
                                                                Submit Order
                                                                <span class="fas fa-paper-plane"></span>
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div aria-hidden="true" aria-labelledby="myModalLabel" class="modal fade" id="modalTakeOrder" role="dialog" tabindex="-1">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header bg-primary white-text">
                                                    <h4 class="title">
                                                        <i class="fas fa-cart-plus"></i> Take Order
                                                    </h4>
                                                    <button aria-label="Close" class="close waves-effect waves-light text-white" data-dismiss="modal" type="button">
                                                        <span aria-hidden="true">
                                                            <span class="fas fa-times-circle"></span>
                                                        </span>
                                                    </button>
                                                </div>
                                                <div class="modal-body mb-0">
                                                    <form id="frmTakeOrder" name="frmTakeOrder" action="#">
                                                        <h5 class="m-b-20">Wallet Information</h5>
                                                        <div class="md-form form-sm">
                                                            <input type="text" class="form-control disabled text-uppercase" id="takeWalletAddress" placeholder="-" tabindex="-1" readonly/>
                                                            <label for="takeWalletAddress">Wallet Address</label>
                                                        </div>
                                                        <div class="md-form form-sm">
                                                            <input type="text" class="form-control disabled text-uppercase" id="takeOrderSellerAddress" placeholder="-" tabindex="-1" readonly/>
                                                            <label for="takeOrderSellerAddress">Seller Address</label>
                                                        </div>
                                                        <h5 class="m-b-20">Token Information</h5>
                                                        <div class="md-form form-sm">
                                                            <input type="text" class="form-control disabled text-uppercase" id="takeTokenAddress" placeholder="-" tabindex="-1" readonly/>
                                                            <label for="takeTokenAddress">Token Address</label>
                                                        </div>
                                                        <div class="md-form form-sm">
                                                            <input type="text" class="form-control disabled" id="takeTokenName" placeholder="-" tabindex="-1" readonly/>
                                                            <label for="takeTokenName">Token Name</label>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="text" class="form-control disabled" id="takeTokenSymbol" placeholder="-" tabindex="-1" readonly/>
                                                                    <label for="takeTokenSymbol">Token Symbol</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="text" class="form-control disabled" id="takeTokenDecimal" placeholder="-" tabindex="-1" readonly/>
                                                                    <label for="takeTokenDecimals">Token Decimals</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h5 class="m-b-20">Your Balance</h5>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="text" class="form-control disabled" id="takeEtherBalance" placeholder="-" tabindex="-1" readonly/>
                                                                    <label for="takeEtherBalance">ETH Balance</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="text" class="form-control disabled" id="takeTokenBalance" value="-" tabindex="-1" readonly/>
                                                                    <label for="takeTokenBalance">Token Balance</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h5 class="m-b-20">Order Details</h5>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="number" class="form-control" id="takeOrderToken" min="1" step="any"  value="-" tabindex="-1" readonly/>
                                                                    <label for="takeOrderToken">Available Tokens</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="number" class="form-control" id="takeOrderPrice" step="any" value="-" tabindex="-1" readonly/>
                                                                    <label for="takeOrderPrice">Price in Ethers</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="number" class="form-control" id="takeTokenAmount" min="1" step="any" />
                                                                    <label for="takeTokenAmount">Tokens to Buy</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="number" class="form-control" id="takeEtherAmount" value="-" tabindex="-1" readonly/>
                                                                    <label for="takeEtherAmount">How many Ether</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="number" class="form-control disabled" id="takeTokenRateEther" value="-" tabindex="-1" readonly/>
                                                                    <label for="takeTokenRateEther">Token Price in Ether</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="md-form form-sm">
                                                                    <input type="number" class="form-control disabled" id="takeTokenRateUsd" value="-" tabindex="-1" readonly/>
                                                                    <label for="takeTokenRateUsd">Token Price in USD</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="md-form-btn text-right">
                                                            <button class="btn btn-primary">
                                                                Buy <span class="takeTokenAmount"></span> Tokens
                                                                <span class="fas fa-paper-plane"></span>
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <hr>
            
                        <div class="clearfix m-b-10">
                            <h2 class="float-left">Open Orders</h2>
                            <h5 class="float-right">Total Orders: <span id="tokenMakeOrdersCount"></span></h5>
                        </div>
                        <section class="card orders-table-container">
                            <div class="orders-table-inner">
                                <div class="table-container table-responsive">
                                    <table class="table table-hover table-responsive-lg">
                                        <thead>
                                            <tr>
                                                <th class=""><span>Token name</span></th>
                                                <!-- <th class=""><span>Token symbol</span></th> -->
                                                <th class="" style="width: 300px; max-width: 300px"><span>Seller Account</span></th>
                                                <th class="text-right" style="width: 125px; max-width: 125px"><span>Total Tokens</span></th>
                                                <!-- <th class="text-right" style="width: 125px; max-width: 125px"><span>Total Amount</span></th> -->
                                                <th class="text-right" style="width: 125px; max-width: 125px"><span>Available Tokens</span></th>
                                                <!-- <th class="text-right" style="width: 125px; max-width: 125px"><span>Sold Tokens</span></th> -->
                                                <th class="text-right" style="width: 150px; max-width: 150px"><span>Price Per Token</span></th>
                                                <!-- <th class=""><span>Buyer Account</span></th> -->
                                                <th class="text-left" style="width: 130px; max-width: 130px"></th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbodyMakeOrders">
                                            
                                        </tbody>
                                    </table>
                                    <div style="display: none">
                                        <table>
                                            <tr id="trMakeOrder">
                                                <td><small class="tokenName"></small></td>
                                                <!-- <td><small class="tokenSymbol"></small></td> -->
                                                <td style="width: 300px; max-width: 300px"><small class="sellerAddress text-uppercase"></small></td>
                                                <td class="text-right" style="width: 125px; max-width: 125px"><small class="numTokens"></small></td>
                                                <!-- <td class="text-right" style="width: 125px; max-width: 125px"><small class="numTokensSold"></small></td> -->
                                                <td class="text-right" style="width: 125px; max-width: 125px"><small class="numTokensAval"></small></td>
                                                <!-- <td class="text-right" style="width: 125px; max-width: 125px"><small class="totalPrice"></small></td> -->
                                                <td class="text-right" style="width: 150px; max-width: 150px"><small class="pricePerToken"></small></td>
                                                <!-- <td><small class="buyerAddress text-uppercase"></small></td> -->
                                                <td class="actions text-left" style="width: 170px; max-width: 170px">
                                                    <a href="#" class="btn btn-sm btn-info btn-take-order btnTakeOrder">Take Order</a>
                                                    <a href="#" class="btn btn-sm btn-danger btn-delete btnDelete">Delete</a>
                                                    <a href="#" class="btn btn-sm btn-danger btn-delete btnDeleteOwner"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="table-footer-container">
                                    <div class="table-length">
                                        <label for="makeOrderCount">Rows per page:</label>
                                        <select name="length" id="makeOrderCount" class="niceSelect">
                                            <option value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="50">50</option>
                                            <option value="-1">All</option>
                                        </select>
                                    </div>
                                    <div class="table-pagination" id="makeOrderPagination">
                                        <ul class="pagination pagination-circle pg-info mb-0">
                                            <li class="page-item" id="makeOrderPrev">
                                                <a class="page-link waves-effect waves-effect" aria-label="Previous">
                                                    <span aria-hidden="true">«</span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                            
                                            <li class="page-item" id="makeOrderNext">
                                                <a class="page-link waves-effect waves-effect" aria-label="Next">
                                                    <span aria-hidden="true">»</span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <hr class="m-t-30">

                        <div class="clearfix m-b-10">
                            <h2 class="float-left">Completed Orders</h2>
                            <h5 class="float-right">Total Orders: <span id="tokenTakeOrdersCount"></span></h5>
                        </div>
                        <section class="card orders-table-container">
                            <div class="orders-table-inner">
                                <div class="table-container table-responsive">
                                    <table class="table table-hover table-responsive-lg">
                                        <thead>
                                            <tr>
                                                <th class=""><span>Token name</span></th>
                                                <th class=""><span>Seller Account</span></th>
                                                <th class="text-right"><span>Total Tokens</span></th>
                                                <th class="text-right"><span>Total Amount</span></th>
                                                <th class="text-right"><span>Price Per Token</span></th>
                                                <!-- <th class=""><span>Buyer Account</span></th> -->
                                            </tr>
                                        </thead>
                                        <tbody id="tbodyTakeOrders">
                                            
                                        </tbody>
                                    </table>
                                    <div style="display: none">
                                        <table>
                                            <tr id="trTakeOrder">
                                                <td><small class="tokenName"></small></td>
                                                <!-- <td><small class="tokenSymbol"></small></td> -->
                                                <td><small class="sellerAddress text-uppercase"></small></td>
                                                <td class="text-right"><small class="numTokens"></small></td>
                                                <td class="text-right"><small class="totalPrice"></small></td>
                                                <td class="text-right"><small class="pricePerToken"></small></td>
                                                <!-- <td><small class="buyerAddress text-uppercase"></small></td> -->
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="table-footer-container">
                                    <div class="table-length">
                                        <label for="takeOrderCount">Rows per page:</label>
                                        <select name="length" id="takeOrderCount" class="niceSelect">
                                            <option value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="50">50</option>
                                            <option value="-1">All</option>
                                        </select>
                                    </div>
                                    <div class="table-pagination" id="takeOrderPagination">
                                        <ul class="pagination pagination-circle pg-info mb-0">
                                            <li class="page-item" id="takeOrderPrev">
                                                <a class="page-link waves-effect waves-effect" aria-label="Previous">
                                                    <span aria-hidden="true">«</span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                            
                                            <li class="page-item" id="takeOrderNext">
                                                <a class="page-link waves-effect waves-effect" aria-label="Next">
                                                    <span aria-hidden="true">»</span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                </main>
            </div> 

        </div>
    </section>

@endsection

@section('styles_truffle')
    
    <!-- Font Families -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,700,700i|Roboto:300,300i,400,400i,700,700i"/>

    <!-- Font Awesome -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.4/js/all.js"></script>

    <!-- Vendors Stylesheets -->
    <link rel="stylesheet" href="{{ asset('truffle/dist/assets/css/vendors.min.css') }}"/>

    <!-- App Stylesheet -->
    <link rel="stylesheet" href="{{ asset('truffle/dist/assets/css/app.css') }}"/>

@endsection


@section('scripts')
    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

    <!-- Vendors Scripts -->
    <script src="{{ asset('truffle/dist/assets/js/vendors.min.js') }}" type="text/javascript"></script>

    <!-- Blockchain Scripts -->
    <script src="{{ asset('truffle/dist/eth/eth.full.min.js') }}?rand=<?php echo rand(); ?>" type="text/javascript"></script>


@endsection

@section('scripts_truffle')
<script>
    $(document).ready(function(){
        Util.base_url = '/truffle/build/contracts';
        ExchangerApp.boot('0x198b9febc1710d7950c02e817c0ab6c2e2f4655c' , '<?php echo $token->address ?>' , '<?php echo $token->network; ?>');
        $(document).trigger('exchangerReady');
    });

    $(document).on('contractsError' , function() {
        $('#exchanger_app').hide();
        $('#exchanger_err').show();
    });

    $(document).on('contractsReady' , function() {
        $('#exchanger_app').show();
        $('#exchanger_err').hide();
    });
</script>
@endsection
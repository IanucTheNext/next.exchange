@extends('_layouts.main')

@section('content')

<div id="app"></div>

<!-- Modal -->
<div class="modal fade" id="user-acceptance" tabindex="-1" role="dialog" style="display: none" aria-labelledby="Modal-Title" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="ModalTitle">Disclaimer</h5>
				<button type="button" class="close not-accepting" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<h4>Caution: Next.exchange is currently in its beta stage.</h4>
				While trade functionality and wallets work, we urge that you please do not place a significant portion of your assets on the platform at the moment due to ongoing testing.
				By accepting this disclaimer, you agree that NEXT is not liable for any loss of assets.

				<br><br>Thank you for your understanding.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary not-accepting">Reject</button>
				<button type="button" id="save_details" class="btn btn-primary">Accept</button>
			</div>
		</div>
	</div>
</div>
</body>
<style>

	.modal-backdrop, .modal-backdrop.fade.in {
		opacity: 0.7;
		filter: alpha(opacity=70);
		background: #000;
	}
</style>

@endsection

@section('scripts_footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.0.1/cleave.min.js"></script>
<!-- Additional JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/async/2.6.0/async.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.0/js/mdb.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<!-- Vendors Scripts --2
<script src="{{ asset('truffle/dist/assets/js/vendors.min.js') }}" type="text/javascript"></script>
<!-- Blockchain Scripts -->
<script src="{{ asset('truffle/dist/eth/eth.token.order.min.js') }}?rand=<?php echo rand(); ?>" type="text/javascript"></script>
<script src="{{ mix('/js/app.js') }}"></script>
<script>
    var coinmarketcaps = {};

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function()
    {
        let flag = "{!! @$user_disclaimer !!}";

        if(flag == 0)
        {
            $('#user-acceptance').modal({backdrop: 'static', keyboard: false})
                .addClass('show')
        }
    });

    $('#save_details').on('click',function(){

        $.ajax({
            "type":"POST",
            "url":'/saveuserdisclaimer',
            "dataType":"json",
            "success":function( response )
            {
                if(response == 1)
                {
                    $('#user-acceptance')
						.removeClass('show')
						.modal("hide");
                }
            }
        });
    });

    $('.not-accepting').on('click',function(){

        $('#user-acceptance')
            .removeClass('show');
        window.location.href	= '/';
    });


// On ICO page
        $('[data-card]').on('click', function() {
            $(this).toggleClass('card--open');
        });

</script>
@endsection


import Aside from './Aside.vue'
import Breadcrumb from './Breadcrumb.vue'
import PageHeader from './PageHeader.vue'
import Callout from './Callout.vue'
import Footer from './Footer.vue'
import Header from './Header.vue'
import Sidebar from './Sidebar/Sidebar.vue'
import Switch from './Switch.vue'
import Admin from './Admin.vue'

export {
  Aside,
  PageHeader,
  Breadcrumb,
  Callout,
  Footer,
  Header,
  Sidebar,
  Switch,
  Admin
}

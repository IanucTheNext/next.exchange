export default {
  items: [
    /*{
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer'
    },*/
      {
          name: 'Markets',
          url: '/markets',
          icon: 'icon-graph'
      },
      {
        name: 'Wallet',
          url: '/wallet',
          icon: 'icon-wallet'
      },
      {
          name: 'Orderbook',
          url: '/orderbook',
          icon: 'icon-chart'
      },
      {
          name: 'Transactions',
          url: '/transactions',
          icon: 'icon-book-open'
      },
      {
          name: 'Exchange',
          url: '/exchange',
          icon: 'fa fa-exchange fa-fw'
      },
      {
          name: 'Token Market',
          url: '/tokenmarket',
          icon: 'fa fa-random fa-fw'
      },
      {
          name: 'ICO',
          url: '/ico',
          icon: 'fa fa-rocket fa-fw'
      },
      {
          name: 'Minepool',
          url: '/minepool',
          icon: 'fa fa-gavel fa-fw'
      },
      {
          name: 'Community',
          url: '/community',
          icon: 'fa fa-users fa-fw'
      },
      /* Admin functions */
      {
          name: 'Blacklist IP',
          url: '/black_list',
          icon: 'icon-wallet',
          isAdmin:true
      },
      {
          name: 'User List',
          url: '/user_list',
          icon: 'icon-user',
          isAdmin:true
      },
      {
          name: 'Access List',
          url: '/access',
          icon: 'icon-user',
          isAdmin:true
      },
      {
          name: 'Logout',
          url: '/logout',
          icon: 'fa fa-sign-out'
      },
  ]
}
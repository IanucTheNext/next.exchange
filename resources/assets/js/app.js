require('./bootstrap');
window.Vue = require('vue');

import Country from './country';

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

const VueResource = require('vue-resource');
Vue.use(VueResource);

window.eventApp = new Vue();

Vue.http.options.emulateJSON = true

Vue.http.interceptors.push((request, next) => {
    request.headers['Access-Control-Allow-Origin'] = '*';
    request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

next();
});


import VueNotifications from 'vue-notification';
Vue.use(VueNotifications);


import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);

import VueSweetAlert from 'vue-sweetalert2';
Vue.use(VueSweetAlert);

import VueClipBoard from 'vue-clipboard2';
Vue.use(VueClipBoard);

import vSelect from 'vue-select';
Vue.component('v-select', vSelect);

import router from './router'
import App from './components/App.vue'
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.

 Vue.component('example', require('./components/Example.vue'));
 Vue.component('task-list', require('./components/TaskList.vue'));
 **/

Vue.component('example', require('./components/Example.vue'));
Vue.component('chat-message', require('./components/ChatMessage.vue'));
Vue.component('chat-log', require('./components/ChatLog.vue'));
Vue.component('chat-composer', require('./components/ChatComposer.vue'));


/*** EXCHANGE APP ***/
Vue.component('example', require('./components/Example.vue'));

/*** EXCHANGE APP ***/
new Vue({
        el: '#app',
        data: {
            btcdeposit: true
        },
        router,
        render: h => h(App)
});


/**** CHAT APP ****/
new Vue({
    el: '#chat',
    data: {
        messages: [],
        usersInRoom: []
    },
    methods: {
        addMessage(message) {
            // Add to existing message
            this.messages.push(message);
            // Persist to the database etc
            axios.post('/messages', message).then(response => {

            })
        }
    },
    created() {
        axios.get('/messages').then(response => {
            this.messages = response.data;
        });
        Echo.join('chatroom')
            .here((users) => {
                this.usersInRoom = users;
            })
            .joining((user) => {
                this.usersInRoom.push(user);
            } )
            .leaving((user) => {
            this.usersInRoom = this.usersInRoom.filter(u => u != user)
            })
            .listen('ChatMessagePosted', (e) => {
                this.messages.push({
                    message: e.message.message,
                    user: e.user
                });
            });
    }
});

<?php

namespace App\Jobs;

use App\Models\Addresses;
use App\Models\Coin;
use App\Models\Fee;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Services\Blockchainservice;
use App\Traits\WalletTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class TransactionsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, WalletTrait;

    private $_orderID;

    /**
     * Create a new job instance.
     *
     * @param $order_id
     */
    public function __construct($order_id)
    {
        $this->_orderID   = $order_id;
    }

    /**
     * Execute the job.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::select("CALL order_execute( '". $this->_orderID . "', @return_data)");

        $result = DB::select('select @return_data as return_data');
        $result = json_decode($result[0]->return_data);

        // Publishing the activity. Client side listeners will read this.
        $redis  = Redis::connection();
        $redis->publish('update-order-vue', json_encode(['event' => 'user_transactions', 'data' => $result]));

        //If no orders executed return from here
        if(!$result) return false;

        $blockChain = new Blockchainservice;
        $fiats  = [10, 11];
        $tx_id  = null;
        $to_address = null;

        $transfer_fee_percentage    = Fee::getFee( 'transfer' );
        $ETHCoins           = Coin::getETHCoins();

        //Looping through the transactions which are returned from the MySQL Procedure
        foreach ($result as $transaction_order) {

            //if BUY need to find amount of main coin used, if SELL executed amount of  market coin
            $total_amount   = $transaction_order->order_buysell == 1 ? bcmul($transaction_order->order_price, $transaction_order->order_amount, 9) : $transaction_order->order_amount;
            $gas_fee            = $blockChain->getFee( $total_amount );
            $total_gas          = bcmul($gas_fee['data'], 2, 9);
            $user_id            = $transaction_order->user_id;
            $wallet_updated     = false;

            try {

                $transaction_confirmations  = 0;

                /**
                 * if Market is NEXT we are not charging any transaction fees
                 */
                if ($transaction_order->coin_id == 13) {

                    $fee_amount     = 0;
                } else {

                    $fee_amount     = bcmul($total_amount, bcdiv($transfer_fee_percentage, 100, 9), 9);
                }

                if(! in_array( $transaction_order->coin_id, $fiats )) {

                    $blockChain->setCoin( $transaction_order->coin_id );

                    /**
                     * if ETH Market coin reducing gas fee from ETH and updates the ETH Wallet
                     * Otherwise it will  deduct from the Market itself
                     * while calculating the total amount to transfer we will deduct the gas fee for our fees transaction
                     * from the user itself, i.e; else part
                     */
                    if (in_array($transaction_order->coin_id, $ETHCoins)) {

                        $wallet             = Wallet::where('user_id', $user_id)
                            ->where('coin_id', 2)
                            ->first();

                        $wallet->amount         = bcsub($wallet->amount, $total_gas, 9);
                        $wallet->amount_inorder = bcsub($wallet->amount_inorder, $total_gas, 9);
                        $wallet->save();

                        $wallet_updated     = true;
                        $blockChain_credit  = bcsub($total_amount, $fee_amount, 9);
                    } else {

                        $blockChain_credit  = bcsub(bcsub($total_amount, $fee_amount, 9), $gas_fee);
                    }

                    //Getting the to and from address
                    $from_address       = Addresses::getAddressByUserID( $user_id, $transaction_order->coin_id );
                    $to_address         = Addresses::getAddressByUserID( $transaction_order->to_user_id, $transaction_order->coin_id);

                    $transfer           = $blockChain->transfer($from_address->pk, $from_address->address_address, $to_address->address_address, $blockChain_credit);

                    if( $transfer ) {

                        $tx_id  = $transfer['data'];

                        //Writing the Fees transactions to table
                        $this->executeFees($blockChain, $transaction_order->order_id, $fee_amount, $gas_fee['data'], $from_address, $transaction_order->coin_id);
                        $transaction_confirmations  = 1;
                    }

                    $actual_credit      = number_format( ($blockChain_credit - $gas_fee['data']), 9);
                } else {

                    $actual_credit      =  number_format( ($total_amount - $fee_amount), 9);
                }

                $transaction                            = Transaction::find( $transaction_order->transaction_id );
                $transaction->transaction_addr          = $to_address != null ? $to_address->address_address : ''; //Transfer to which address
                $transaction->transaction_amount        = $actual_credit;
                $transaction->transaction_rx_id         = $tx_id;
                $transaction->transaction_fee           = $fee_amount;
                $transaction->gas_fee_amount            = bcmul($gas_fee['data'], 2, 9);
                $transaction->transaction_confirmations = $transaction_confirmations;
                $transaction->transaction_type          = 3;
                $transaction->save();
            } catch (\Exception $e) {

                Log::error(json_encode([

                    'error_code'    => $e->getCode(),
                    'message'       => $e->getMessage(),
                    'line_no'       => $e->getLine()
                ]));

                if($wallet_updated) {

                    $wallet             = Wallet::where('user_id', $user_id)
                        ->where('coin_id', 2)
                        ->first();

                    $wallet->amount         = bcadd($wallet->amount, $total_gas, 9);
                    $wallet->amount_inorder = bcadd($wallet->amount_inorder, $total_gas, 9);
                    $wallet->save();
                }
            }

        }
    }
}

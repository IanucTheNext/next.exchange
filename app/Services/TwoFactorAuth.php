<?php

namespace App\Services;

use Srmklive\Authy\Contracts\Auth\TwoFactor\Authenticatable;
use Srmklive\Authy\Contracts\Auth\TwoFactor\Provider;
use Srmklive\Authy\Services\Authy;

class TwoFactorAuth
{
    /**
     * TwoFactor auth provider object.
     *
     * @var Provider[]
     */
    private $providers = [
        'authy' => Authy::class,
        'google' => GoogleAuthenticator::class,
    ];

    /**
     * @var string
     */
    private $currentProvider = 'authy';

    /**
     * Get specific TwoFactor auth provider object to use.
     *
     * @param null|string $name
     * @return Provider
     */
    public function getProvider($name = null)
    {
        $name = $name ?: $this->currentProvider;
        $this->initProvider($name);
        return @$this->providers[$name];
    }

    /**
     * Set specific TwoFactor auth provider to use.
     *
     * @param string $name
     *
     * @return Provider
     */
    public function setProvider($name)
    {

        return self::getProvider($name);
    }

    /**
     * @param Authenticatable $user
     * @return bool
     */
    public function isEnabled(Authenticatable $user)
    {
        return (bool)$this->getEnabled($user);
    }

    /**
     * @param Authenticatable $user
     * @return Provider
     */
    public function getEnabled(Authenticatable $user)
    {
        foreach ($this->providers as $name => $provider) {
            $provider = $this->getProvider($name);
            if ($provider->isEnabled($user)) {
                return $provider;
            }
        }

        return null;
    }

    /**
     * @param $name
     */
    private function initProvider($name)
    {
        if (!$this->isProviderInitialized($name)) {
            $this->providers[$name] = app($this->providers[$name]);
        }
    }

    /**
     * Determines if 2fa provider initialized.
     *
     * @param $name
     * @return bool
     */
    private function isProviderInitialized($name)
    {
        return $this->providers[$name] instanceof Provider;
    }
}
<?php
namespace App\Services;

use App\Helpers\Helper;
use App\Models\Coin;
use GuzzleHttp\Client;
use App\Models\Addresses;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Auth;

class Blockchainservice {
	
	private $nodeServer;

	private $_coinId;
	private $_coin;

	public function __construct( )
    {
        $this->nodeServer = env("COIN_NODE_SERVER", "http://localhost:3000");

    }

    public function setCoin( $coin_id ) {

        $this->_coinId   = $coin_id;


    }
    public static function getTransactionList($symbol, $address){
        $client = new Client(); 

        if(strlen($address) && strlen($symbol)){
            $uri = (new self)->nodeServer.'/transaction/list/'.$symbol.'/'.$address;
        }
        
        try {
            $res = $client->request('GET', $uri);
            $result = json_decode($res->getBody()->getContents(), true);

            return $result;
        } catch(GuzzleException $exception) {

            $result_error = [
                'status'    => false,
                'error'     => $exception->getMessage(),
                'uri'       => $uri
            ];
            return $result_error;
        }

    }

    public static function getCoinBalance( $coin_name, $coin_address, $contract_address = null ) {

        $client = new Client(); //GuzzleHttp\Client

        if(! strlen($contract_address) ) {

            $uri    = (new self)->nodeServer . '/wallet/balance/'.strtolower($coin_name).'/'.$coin_address;
        } else {

            $uri    = (new self)->nodeServer . '/wallet/balance/erc20/'.$coin_address.'?contract='.$contract_address;
        }

        try {

            $res = $client->request('GET', $uri);
            $result = json_decode($res->getBody()->getContents(), true);

            return $result;
        } catch(GuzzleException $exception) {

            $result_error = [
                'status'    => false,
                'coin_name' => $coin_name,
                'error'     => $exception->getMessage(),
                'uri'       => $uri
            ];
            Helper::LogError( 'Get Balance from BlockChain Error: - ' . json_encode($result_error) );
            return $result_error;
        }

    }

	public function getBalance(  )
	{

		$currentUser = Auth::user();
        $current_address = Addresses::getAddressByUserID($currentUser->id, $this->_coinId);

        if(!$current_address) return false;

        $coin_name = $this->_getCoinNameByID();
        if ($coin_name === '') {
        	return false;
        }

        $contract_address = $this->_getSmartContractAddress();

        $client = new Client(); //GuzzleHttp\Client
        $uri = $this->nodeServer . '/wallet/balance/'.$coin_name.'/'.$current_address->address_address.'?contract='.$contract_address;

        $res = $client->request('GET', $uri);

        $res_status = $res->getStatusCode();
        $result = json_decode($res->getBody()->getContents(), true);

        if ($res_status == 200) {

            return $result;
        } else {

            return false;
        }
	}

	public function createAddress( )
	{
		$new_address    = [

		    'pk'        => null,
            'address'   => null
        ];

		$coin_name      = $this->_getCoinNameByID();

		if ($coin_name === '') {
		    
        	return false;
        }

        $uri        = $this->nodeServer . '/wallet/create/'.$coin_name;

        try {

            $client     = new Client(); //GuzzleHttp\Client

            $res        = $client->request('POST', $uri);
            $res_status = $res->getStatusCode();

            $result     = json_decode($res->getBody()->getContents(), true);

            if ($res_status == 200 && $result['status']) {

                $new_address['address'] = $result['data']['addr'];
                $new_address['pk'] = $result['data']['pk'];
            }

        } catch (GuzzleException $exception) {

            $result_error = [
                'status'    => false,
                'coin_id'   => $this->_coinId,
                'error'     => $exception->getMessage(),
                'uri'       => $uri
            ];
            Helper::LogError('Generate Address BlockChain Error: - ' . json_encode($result_error));
        }

        return $new_address;
	}

	public function transfer($coin, $pk, $from, $to, $amount) {


        $client = new Client(); //GuzzleHttp\Client
        $uri = $this->nodeServer . '/transaction/create/'.$coin;

        $data = [

            'pk'        => $pk,
            'from'      => $from,
            'to'        => $to,
            'amount'    => $amount
        ];

        if($coin == 'erc20') {

            $data['contract'] = $this->_getSmartContractAddress();
        }

        try {

            $res        = $client->request('POST', $uri, ['form_params' => $data]);
            $res_status = $res->getStatusCode();

            $result     = json_decode($res->getBody()->getContents(), true);

            if ($res_status == 200) {

                return $result;
            } else {

                return false;
            }
        } catch (GuzzleException $exception) {

            $result_error = [
                'status'    => false,
                'coin'      => $coin,
                'error'     => $exception->getMessage(),
                'uri'       => $uri,
                'data'      => $data
            ];
            Helper::LogError('Transaction BlockChain Error: - ' . json_encode($result_error));
            return false;
        }

	}

    public function getFee($amount) {

        $coin_name = $this->_getCoinNameByID();

        if ($coin_name == '') {
            return false;
        }

        $uri = $this->nodeServer . '/transaction/fee/'.$coin_name.'?amount='.$amount;

        try {

            $client = new Client(); //GuzzleHttp\Client


            $res = $client->request('GET', $uri);
            $result     = json_decode($res->getBody()->getContents(), true);

            return $result;
        } catch(GuzzleException $exception) {

            $result_error = [
                'status'    => false,
                'coin_id'   => $this->_coinId,
                'error'     => $exception->getMessage(),
                'uri'       => $uri
            ];
            Helper::LogError('Transaction BlockChain Error: - ' . json_encode($result_error));
            return $result_error;
        }
    }

    private function _getSmartContractAddress() {

        return Coin::getContractAddress( $this->_coinId );
    }

	private function _getCoinNameByID() {
	       
        return Coin::getNameOrTypeByID( $this->_coinId );
	}
}
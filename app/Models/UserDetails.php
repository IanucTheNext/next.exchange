<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{


    protected $table = 'users_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'fullname', 'occupation', 'company', 'phone', 'address', 'city', 'state', 'postcode', 'country', 'bitcoin', 'ether', 'litecoin', 'facebook', 'linkedin', 'twitter', 'instagram'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function user() {
        return $this->BelongsTo(User::class);
    }



}

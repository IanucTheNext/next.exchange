<?php

namespace App\Models;

use App\Helpers\Helper;
use App\Services\Blockchainservice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class Wallet extends Model
{

	protected $table = 'wallet';

    protected $fillable = [
        'tx_id', 'user_id', 'coin_id', 'name', 'amount'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * Set the wallet database from block chain values
     *
     * @param $user_id
     * @param $coin_id
     * @param $coin_name
     * @param $newBalance
     * @return bool
     */
   public static function setFromBlockChain( $user_id, $coin_id, $coin_name, $newBalance) {

       $wallet  = Wallet::where('user_id', $user_id)
           ->where('coin_id', $coin_id)
           ->first();

       //Checking wallet exists or not
       if($wallet) {

           $wallet->amount  = $newBalance;
           $wallet->save();
       } else {

           Wallet::create([

               'tx_id'     => Helper::uniqueID(),
               'user_id'   => $user_id,
               'coin_id'   => $coin_id,
               'name'      => $coin_name,
               'amount'    => $newBalance,
               'amount_inorder' => 0
           ]);
       }

       unset($wallet);

       return true;
   }

    /**
     * Get wallet and address details of user's active wallet coins
     *
     * @param $user_id
     * @return array
     */
   public static function getWalletWithAddress( $user_id ) {

       $enabled_coins           = [];
       $btcAddress              = Addresses::getAddressByUserID( $user_id, 1 );
       $ethAddress              = Addresses::getAddressByUserID( $user_id, 2 );
       $xemAdddress             = Addresses::getAddressByUserID( $user_id, 9 );

       $wallets  = Wallet::leftJoin('addresses', function ( $join ) {
           $join->on('wallet.coin_id', 'addresses.address_coin');
           $join->on('wallet.user_id', 'addresses.address_user_id');
       })
           ->join('coins', 'coins.coin_id', '=', 'wallet.coin_id')
           ->where('wallet.wallet_enabled', 1)
           ->where('wallet.user_id', $user_id)
           ->orderBy('coins.coin_id')
           ->get([
               'wallet.id AS wallet_id',
               'wallet.coin_id AS coin_id',
               'amount',
               'amount_inorder',
               'address_address',
               'coin_coin',
               'coin_max_withdraw',
               'coin_erc',
               'coin_fiat',
               'coin_market',
               'coin_address'
           ]);

       //Terminating here if no rows exists based on our conditions
       if( !$wallets || !count($wallets) ) return response()->json( $enabled_coins );

       //Creating the coin details array
       foreach( $wallets as $wallet ) {

           if($wallet->coin_fiat == 0) {

               $available_balance   = bcsub($wallet->amount, $wallet->amount_inorder, 9);
           } else {

               $available_balance   = bcsub($wallet->amount, $wallet->amount_inorder, 2);
           }

           $wallet_address  = [
               'coin_id'            => $wallet->coin_id,
               'symbol'             => $wallet->coin_coin,
               'amount'             => $wallet->amount,
               'amount_inorder'     => $wallet->amount_inorder,
               'available'          => $available_balance,
               'payment_amount'     => null,
               'payment_address'    => null,
               'show_withdraw'      => false,
               'show_deposit'       => false,
               'show_data'          => 1,
               'total_fee'          => 0,
               'credit_amount'      => 0,
               'class'              => '',
               'daily_limit'        => $wallet->coin_max_withdraw,
               'btn_disabled'       => false,
               'error'              => '',
               'maintenance_mode'   => false
           ];

           if($wallet->coin_market  == 'ETH') {
               $wallet['address']   = $ethAddress->address_address;
           }

           switch ( $wallet->coin_market ) {

               case 'BTC': $wallet_address['address']   = $btcAddress->address_address; break;
               case 'ETH': $wallet_address['address']   = $ethAddress->address_address; break;
               case 'XEM': $wallet_address['address']   = $xemAdddress->address_address; break;
               default:    $wallet_address['address']   = $wallet->address_address; break;
           }

           if($wallet->coin_id == 13) {

               $wallet_address['show_details']  = false;
           }

           if( $wallet->coin_fiat == 0 ) {

               $wallet_blockchain   = Blockchainservice::getCoinBalance( $wallet->coin_coin, $wallet_address['address'], $wallet->coin_address);

               if($wallet_blockchain['status'] == true) {

                   $wallet_address['amount']    = number_format($wallet_blockchain['data'], 9);
                   $wallet_address['available'] = bcsub($wallet_address['amount'], $wallet->amount_inorder, 9);

                   $updateWallet            = Wallet::find($wallet->wallet_id);
                   $updateWallet->amount    = number_format($wallet_blockchain['data'], 9);
                   $updateWallet->save();
                   unset($updateWallet);
               } else {

                   $wallet_address['maintenance_mode']  = true;
               }
           }

           $coin_market = $wallet->coin_market != '' ? $wallet->coin_market : 'general_market';

           $enabled_coins[$coin_market][$wallet->coin_id]   = $wallet_address;
       }

       return $enabled_coins;
   }

    /**
     * Returns walletID by userID and coinID
     *
     * @param $user_id
     * @param $coin_id
     * @return mixed
     */
    public static function walletIDbyUserAndCoinID( $user_id, $coin_id ) {

        return Wallet::select('id')
            ->where('user_id', $user_id)
            ->where('coin_id', $coin_id)
            ->first();
    }

    /**
     * Fetch Wallet balance based on coin and user
     *
     * @param $coin_id
     * @param $user_id
     * @return array
     */
    public static function getBalance( $coin_id, $user_id )
    {

        try {
            $available  = Wallet::select(['amount', 'amount_inorder'])
                ->where('coin_id', $coin_id)
                ->where('user_id', $user_id)
                ->first();

            if($available)
            {
                return [
                    'amount'    => $available->amount,
                    'inorder'   => $available->amount_inorder
                ];
            }

            return [
                'amount'    => 0,
                'inorder'   => 0
            ];
        } catch (Exception $e)  {

            return $e->getMessage();
        }
    }

    /**
     * Update the wallet balance when order is deleting.
     *
     * @param $coin
     * @param $amount_in_order
     */
    public static function updateWalletForDeletedOrder( $coin, $amount_in_order) {

        Wallet::where('coin_id' , $coin)
            ->where('user_id', Auth::id())
            ->update(['amount_inorder' => DB::raw("amount_inorder - {$amount_in_order}")]);

    }

    /**
     * Foreign key relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->BelongsTo(User::class);
    }

    public function token() {
        //return $this->BelongsTo(Token::class, 'token_id', 'id');
    }

    /**
     * Returns all Currencies with wallet balance if wallet balance is not zero
     *
     * @return mixed
     */
    public static function currencyCoins() {

        $currencies  = Coin::select(
                'coins.coin_id AS coin_id',
                'coins.coin_coin AS coin_coin',
                DB::raw('(amount - amount_inorder) AS current_balance')
            )
            ->join('wallet', 'coins.coin_id', '=', 'wallet.coin_id')
            ->where('wallet.user_id', Auth::id())
            ->where('coins.coin_enabled', 1)
            ->where(DB::raw('(amount - amount_inorder)'), '>', 0)
            ->get();

        return $currencies;
    }


}

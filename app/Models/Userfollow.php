<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userfollow extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_follows';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Fillable fields for a User_Like
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'follower_user_id',
    ];

    protected $casts = [

    ];

    /**
     * A User_Like belongs to a user
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }


}

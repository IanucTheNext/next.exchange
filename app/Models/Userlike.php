<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userlike extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_likes';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Fillable fields for a User_Like
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'liked_user_id',
    ];

    protected $casts = [

    ];

    /**
     * A User_Like belongs to a user
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }


}

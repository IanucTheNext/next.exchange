<?php

namespace App\Models;

use App\Helpers\Helper;
use App\Services\Blockchainservice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use function Sodium\add;

//use Illuminate\Database\Eloquent\SoftDeletes;

class Addresses extends Model
{
    //use SoftDeletes;

    protected $table = 'addresses';

    protected $primaryKey   = 'address_id';

    protected $fillable = ['address_user_id', 'address_address', 'address_coin', 'address_name', 'address_type', 'pk', 'salt', 'IV'];

    protected $hidden = ['pk', 'salt'];

    public static function getAddressByUserID($user_id, $coin_id)
    {

        $result = Addresses::select('address_address', 'pk')
            ->where('address_coin', $coin_id)
            ->where('address_user_id', $user_id)
            ->first();


        return $result != null ? $result : false;
    }

    /**
     * Returns the block chain address by coin id for current user, if address exits will return that address else
     * Generate new address from Block chain, save to address table and return that address
     *
     * @param $user_id
     * @param $coin_id
     * @return String
     */
    public static function generateAddressByUserID( $user_id, $coin_id )
    {

        $coin   = Coin::getNameByID( $coin_id );
        $getMarket  = Coin::where('coin_id', $coin_id)
            ->orderBy('coin_id', 'asc')
            ->first(['coin_market']);

        //If the $coin_id is from ETH Family and there is no address generated for that family we store
        //2 in this variable(i.e. ETH coin address), we are storing 2 here to generate family's first address under ETH,
        //if the $coin_id not under this family we store the $coin_id itself to $coinToGenerateAddress and its default
        $coinToGenerateAddress   = $coin_id;

        //Getting the ETH related coins from .env file
        $Family          = Coin::getFamilyCoins( $coin_id );


        $address = Addresses::whereIn('address_coin', $Family)
            ->orderBy('address_coin')
            ->first();

        if($address && $Family[0] == $coin_id) {

            $generated_address = Addresses::create([

                'address_user_id'   => $user_id,
                'address_address'   => $address->address_address,
                'address_coin'      => $Family[0],
                'address_name'      => 'Wallet',
                'address_type'      => $getMarket->coin_market != '' ? $getMarket->coin_market : $coin,
                'pk'                => $address->pk
            ]);
            self::createWallet($generated_address->address_id, $user_id, $coin_id, $coin);

            return $generated_address;
        }

        if(in_array( $coin_id, $Family )) {

            $coinToGenerateAddress  = $Family[0];
        }

        //if not BTC, ETH or XEM setting the coin_id as to generate address
        if(!in_array($coinToGenerateAddress, [1, 2, 9])) {

            $coinToGenerateAddress  = $coin_id;
        }

        //Searching existing address for the coin_id, if exists return that address
        $address    = Addresses::where( 'address_user_id', $user_id )
            ->where( 'address_coin', $coinToGenerateAddress )
            ->first();

        if( $address ) {

            $wallet = $wallet     = Wallet::where('user_id', $user_id)
                ->where('coin_id', $coin_id)
                ->first();

            if(! $wallet) {

                self::createWallet($address->address_id, $user_id, $coin_id, $coin);
            }

            return $address;
        }
        //Releasing from memory
        unset($address);

        /*If no address exists for coins, check the address in ETH family exists, if found will return that address*/

        //Else Generate address from BlockChain and write to Addresses table with coin and user ID
        $blockChain = new Blockchainservice;
        $blockChain->setCoin( $coinToGenerateAddress );
        $address    = $blockChain->createAddress();

        if(! $address ) return null;

        if(! $address['address'] ) return null;

        //Writing to table and returns the address
        $generated_address = Addresses::create([

            'address_user_id'   => $user_id,
            'address_address'   => $address['address'],
            'address_coin'      => $coinToGenerateAddress,
            'address_name'      => 'Wallet',
            'address_type'      => $coinToGenerateAddress == 2 ? 'ETH' : $coin,
            'pk'                => $address['pk']
        ]);

        self::createWallet($generated_address->address_id, $user_id, $coin_id, $coin);
        return $generated_address;
    }

    /**
     * Search for a payment id in address table
     *
     * @param $address_payment_id
     * @return bool
     */
    public static function searchPaymentID( $address_payment_id ) {

        $exists = Addresses::where('address_payment_id', $address_payment_id)->first();

        if($exists) {
            return true;
        }

        return false;
    }

    /**
     * Creating an empty wallet
     *
     * @param $address_id
     * @param $user_id
     * @param $coin_id
     * @param $coin_name
     * @return bool
     */
    private static function createWallet($address_id, $user_id, $coin_id, $coin_name) {

        $wallet     = Wallet::where('coin_id', $coin_id)
            ->where('user_id', $user_id)
            ->first();

        if(! $wallet) {

            $wallet     = new Wallet;

            $wallet->tx_id          = Helper::uniqueID();
            $wallet->address_id     = $address_id;
            $wallet->user_id        = $user_id;
            $wallet->coin_id        = $coin_id;
            $wallet->name           = $coin_name;
            $wallet->amount         = 0.0;
            $wallet->save();
        } else {

            $wallet->address_id     = $address_id;
            $wallet->save();
        }

        return true;
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ico extends Model
{
	
	protected $table = 'ico';

    protected $fillable = [
        'name', 'symbol', 'total_supply_token', 'stage', 'launch_date', 'initial_price', 'short_description', 'full_description', 'website_url', 'whitepaper_url', 'twitter_url', 'facebook_url', 'telegram_url', 'bitcointalk_url', 'official_video_url'
    ];
}

<?php

namespace App\Models;

use Srmklive\Authy\Auth\TwoFactor\Authenticatable as TwoFactorAuthenticatable;
use Srmklive\Authy\Contracts\Auth\TwoFactor\Authenticatable as TwoFactorAuthenticatableContract;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Access;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\Helper;

class User extends Authenticatable implements TwoFactorAuthenticatableContract
{
    use Notifiable;
    use HasRoleAndPermission; // To bring in the assignrole functions
    use TwoFactorAuthenticatable;
    use SoftDeletes;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'first_name',
        'last_name',
        'email',
        'password',
        'referred_by',
        'token',
        'activated',
        'signup_ip_address',
        'signup_confirmation_ip_address',
        'signup_sm_ip_address',
        'admin_ip_address',
        'updated_ip_address',
        'deleted_ip_address',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_options',
        'activated',
        'token'
    ];

    protected $dates = [
        'deleted_at'
    ];

    //public function referrals() {
        //return $this->belongsToMany('App\User');
    //
    //
    //}

    public function getReferral($user, $program) {
        return url('https://next.exchange?ref='.$this->uri);
    }

    public function details() {
        return $this->hasOne(UserDetails::class);
    }

    public function wallet() {
        return $this->hasMany(Wallet::class);
    }
    public function social()
    {
        return $this->hasMany('App\Models\Social');
    }

    /**
     * User Profile Relationships
     *
     * @var array
     */
    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }
    // User Profile Setup - SHould move these to a trait or interface...
    public function profiles()
    {
        return $this->belongsToMany('App\Models\Profile')->withTimestamps();
    }
    public function hasProfile($name)
    {
        foreach($this->profiles as $profile)
        {
            if($profile->name == $name) return true;
        }
        return false;
    }
    public function assignProfile($profile)
    {
        return $this->profiles()->attach($profile);
    }
    public function removeProfile($profile)
    {
        return $this->profiles()->detach($profile);
    }

    public function isAdmin()
    {
        return $this->role === 'admin';
    }

    public function isTokenHolder() {
       return $this->access()->where('active', '=', 1)->first();
    }

    /*
     * ACCESS
     */

    public function access()
    {
        return $this->hasOne(\App\Models\Access::class);
    }

    /*
     * User ip's
     */
    public function user_ips()
    {
        return $this->hasOne(UsersIp::class);
    }

    /*
     * Make relation between tables users and users_likes
    */
    public function userlike()
    {
        return $this->hasMany('App\Models\Userlike');
    }

    /*
     * Make relation between tables users and users_follows
    */
    public function userfollow()
    {
        return $this->hasMany('App\Models\Userfollow');
    }

    /*
     * Make relation between tables users and users_connections
    */
    public function userconnection()
    {
        return $this->hasMany('App\Models\Userconnection');
    }

    /*
     * Make relation between tables users and posts
    */
    public function post()
    {
        return $this->hasMany('App\Models\Post');
    }

    public function chatMessages() {
        return $this->hasMany(ChatMessage::class);
    }
}

<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Blockchain\Ethereum\Types\BlockNumber;
use App\Models\Addresses;
use App\Models\Coin;
use App\Models\Fee;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use App\Notifications\SuccessfulTransaction;
use App\Services\ETNService;
use App\Traits\WalletTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Blockchain\EthereumController;
use App\Http\Controllers\Blockchain\Ethereum\EthereumClient;
use App\Http\Controllers\Blockchain\Ethereum\Types\Address as Address;
use App\Http\Controllers\Blockchain\Ethereum\Types\Ether as Ether;
use App\Services\Blockchainservice;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;


class CryptoController extends Controller
{
    use WalletTrait;

    /**
     * Generating the new Coin address
     *
     * @param null $coin_id
     * @return null|String
     */
    public function getAddressNew( $coin_id = null ){

        if(is_null( $coin_id )) return $coin_id;

        return Addresses::generateAddressByUserID( Auth::id(), $coin_id );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function withdraw(Request $request) { // ToDo: Add order and sync local wallet

        $input      = $request->all();

        $coin_id    = $input['coinid'];
        $to         = $input['toaddress'];
        $amount     = $input['amount'];
        $show_data  = $input['show_data'];

        $coin_name  = $coin       = Coin::getNameOrTypeByID( $coin_id );
        if($coin == 'erc20') {

            $coin_name = Coin::getNameByID( $coin_id );
        }

        if($coin == 'erc20') {
            $exist_address = Addresses::getAddressByUserID(auth()->user()->id, 2);
        } else {
            $exist_address = Addresses::getAddressByUserID(auth()->user()->id, $coin_id);
        }

        if(!$exist_address) {

            return response()->json([
                'status'    => false,
                'message'   => 'Your do not have ' . $coin . ' address in our database'
            ]);
        }

        $pk     = $exist_address->pk;
        $from   = $exist_address->address_address;

        if($from == $to) {

            return response()->json([
                'status'    => false,
                'message'   => 'Withdraw to same address not permitted.'
            ]);
        }

        switch($amount) {

            case '':
            case '0':

                return response()->json([
                    'status'    => false,
                    'message'   => 'Please enter a valid amount to withdraw '
                ]);
        }

        $withdraw_limit  = Coin::getDailyWithdrawLimit( $coin_id );
        //Setting withdraw limit by coin_id, if -1 => no limit for withdraw
        if($withdraw_limit != -1) {

            if( $amount > $withdraw_limit ) {

                return response()->json([
                    'status'    => false,
                    'message'   => 'You can\'t withdraw an amount greater than ' . $withdraw_limit
                ]);
            }
        }

        $wallet         = Wallet::getBalance( $coin_id, Auth::id() );
        $wallet_balance = bcsub( $wallet['amount'], $wallet['inorder'],9 );

        //Checking sufficient Balance is available
        if( $wallet_balance < $amount ) {

            return response()->json([
                'status'    => false,
                'message'   => 'Insufficient Balance in your wallet'
            ]);
        }

        if($withdraw_limit != -1) {

            $daily_transaction_amount = Transaction::getDailyTransactionAmount(Auth::id(), $coin_id, 2);
            if ($daily_transaction_amount >= $withdraw_limit) {

                return response()->json([
                    'status' => false,
                    'message' => 'You already withdraw ' . $daily_transaction_amount . ' of ' . $withdraw_limit . ' ' . $coin
                ]);
            }
        }

        $blockchain_service = new Blockchainservice;
        $blockchain_service->setCoin( $coin_id );
        //Get Gas Fees
        $gasFee         = $blockchain_service->getFee( $amount );

        if(! $gasFee['status']) {

            Log::error('Gas Fee Error:');
            Log::error(json_encode($gasFee));

            return response()->json([
                'status'    => false,
                'message'   => "You can't withdraw " . $coin_name . " this time, Please try again later"
            ]);
        }

        $withdraw_fee_percentage    = Fee::getFee('withdraw');
        $withdraw_fee               = bcmul($amount, bcdiv($withdraw_fee_percentage, 100, 9), 9);

        if($coin_id == 12) {

            $total_fee      = $gasFee['data'] . ' ETH';
            $credit_to_user = $amount;

        } elseif( $coin == 'erc20') {

            $total_fee      = $gasFee['data'] . ' ETH & ' . $withdraw_fee . ' ' . $coin;
            $credit_to_user = bcsub($amount, $withdraw_fee, 9);

        } else {

            //Adding 1 first then subtracting after getting the sum, Because the decimal numbers like .00001 is not getting exact precision while adding with another fractional number.
            $total_fee                  = bcsub(bcadd(1 + $gasFee['data'], $withdraw_fee, 9), 1, 9);
            $credit_to_user             = bcsub($amount, $total_fee, 9);
        }

        if($credit_to_user <= 0) {

            return response()->json([
                'status'    => false,
                'message'   => "You don't have enough " . $coin_name . " to pay the fee of " . $total_fee
            ]);
        }

        if($show_data == 1) {

            return response()->json([

                'status'        => 'show_data',
                'amount'        => $amount,
                'total_fee'     => $total_fee,
                'credit_amount' => $credit_to_user
            ]);
        }

        $amount_after_fees  = bcsub($amount, $withdraw_fee, 9);

        $transfer_result    = $blockchain_service->transfer($coin, $pk, $from, $to, $amount_after_fees);

        if(! $transfer_result ) return response()->json([
            'status'    => false,
            'message'   => 'We are not able to process this transaction at this moment, Please contact administrator'
        ]);

        $data   = $this->withDrawFromWallet( Auth::id(), $coin_id, $amount, $transfer_result['data'] );

        if($data) {

            Auth::user()->notify( new SuccessfulTransaction(
                $amount,
                $transfer_result['data'],
                'withdraw',
                $coin
            ));

            $this->checkBeneficiary($to, $coin_id, $coin, $amount_after_fees, $transfer_result['data']);

            $withDrawGas    =  $blockchain_service->getFee( $withdraw_fee );
            $this->executeFees( $blockchain_service, 0, $withdraw_fee, $withDrawGas, $from, $coin_id, 2 );

            return response()->json([
               'status'     => true,
               'message'    => $amount_after_fees . ' '. $coin .' transferred to address ' . $to
            ]);
        }

        return response()->json([
            'status'     => false,
            'message'    => "Can't perform the transaction at this moment pls try again later"
        ]);
    }

    /**
     * Check the Beneficiary address exists in our DB, if exists writing the transaction to our table
     *
     * @param $to
     * @param $coin_id
     * @param $coin
     * @param $amount
     * @param $rx_id
     * @return mixed
     */
    public function checkBeneficiary( $to, $coin_id, $coin, $amount, $rx_id ) {

        $address    = Addresses::where('address_address', $to)
            ->get(['address_id', 'address_user_id']);

        if(! count($address)) return false;

        try {

            $wallet     = Wallet::where('address_id', $address[0]->address_id)
                ->where('coin_id', $coin_id)
                ->get(['id', 'amount']);

            if(!count($wallet)) {

                $error  = [

                    'SQL'           => Wallet::where('address_id', $address[0]->address_id)->where('coin_id', $coin_id)->toSql(),
                    'address'       => $address,
                    'to_address'    => $to,
                    'coin_id'       => $coin_id,
                    'coin_name'     => $coin
                ];

                Helper::LogError( $error );
                return false;
            }

            $wallet_balance = bcadd($wallet[0]->amount, $amount, 9);

            $user   = User::find($address[0]->address_user_id);

            if($coin == 'erc20') {

                $coin = Coin::getNameByID( $coin_id );
            }

            $user->notify( new SuccessfulTransaction(
                $amount,
                $rx_id,
                'deposit',
                $coin
            ));

            return $this->generateTransaction( $address[0]->address_user_id, $wallet[0]->id, $coin_id, $amount, $wallet_balance, $rx_id, 1, 1 );
        } catch (\Exception $exception) {

            $error  = [

                'Message' => $exception->getMessage(),
                'Trace'    => $exception->getTrace()
            ];

            Helper::LogError( $error );
        }

        return false;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use App\Logic\Image\ImageRepository;

use App\Models\User;
use App\Models\UserDetails;
use App\Models\IcoWhitelist;

use App\Http\Requests\ProfileFormRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use Intervention\Image\ImageManagerStatic as Image;



class ProfileController extends Controller
{
    protected $image;

    public function __construct(ImageRepository $imageRepository)
    {
        $this->image = $imageRepository;
    }

    public function update_profile(Request $request)
    {
    }

    public function showUploadFile(Request $request)
    {
        $form_data = Input::all();
        $photo = $form_data['file'];

        /*$file = $request->file('file');
        print_r($request);*/
        //Display File Name
        'File Name: ' . $photo->getClientOriginalName();

        '<br>';

        //Display File Extension
        'File Extension: ' . $photo->getClientOriginalExtension();
        '<br>';

        //Display File Real Path
        'File Real Path: ' . $photo->getRealPath();
        '<br>';

        //Display File Size
        'File Size: ' . $photo->getSize();
        '<br>';

        //Display File Mime Type
        'File Mime Type: ' . $photo->getMimeType();

        //Move Uploaded File
        $destinationPath = 'uploads';
        if ($photo->move($destinationPath, $photo->getClientOriginalName())) {
            User::where('id', Auth::id())->update(['img' => $photo->getClientOriginalName()]);

        }
        $data = DB::table('users')->get()->where('id', Auth::id());
        echo "success";
    }


    public function postUpload()
    {
        $photo = Input::all();
        $response = $this->image->upload($photo);
        return $response;
    }

    public function deleteUpload()
    {

        $filename = Input::get('id');

        if (!$filename) {
            return 0;
        }

        $response = $this->image->delete($filename);

        return $response;
    }

    public function create()
    {
        if (UserDetails::whereUserId(Auth::id())->first()) {
            $data = UserDetails::where('user_id', Auth::id())->first();
        } else {
            $data = User::where('id', Auth::id())->first();
        }
        return view('dashboard.profile', ['user' => $data]);
    }

    public function store(ProfileFormRequest $request)
    {

        $userdetails = UserDetails::whereUserId(Auth::id())->first();

        if (!empty($userdetails)) {
            $userdetails->fill($request->all());
            $userdetails->save();
        } else {
            $request->merge(['user_id' => Auth::id()]);
            UserDetails::create($request->all());
            $request->session()->flash('msg', 'Profile Update Successfully');
        }
        // Todo: put email notification here
        return response()->json(['status' => '200', 'success' => true, 'message' => trans('messages.noty_category_deleted')]);


    }

    public function Getdetails()
    {
        $data = null;

        if (UserDetails::whereUserId(Auth::id())->first()) {
            $data = UserDetails::where('user_id', Auth::id())->get();
            $success = 1;
        } else {
            //$data = User::where('id', Auth::id())->get();
            $success = 0;
        }
        return response()->json(['data' => $data, 'success' => $success]);
    }

    public function idProof()
    {
        $data = UserDetails::where('user_id', Auth::id())->first();
        return view('dashboard.idproof', ['user' => $data]);
    }

    public function icoWhitelist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'eth' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['data' => $validator->messages(), 'success' => 0]);
        } else {
            IcoWhitelist::insert([
                'eth' => $request->eth,
                'user_id' => Auth::id(),
                'ico_id' => $request->ico_id,
                'created_at' => \Carbon\Carbon::now()
            ]);
            return response()->json(['success' => 1, 'message' => "Ethereum address is whitelisted."]);
        }
    }


    public function profileFiles(Request $request)
    {
        $this->validate($request, [
            'address_proof' => 'mimes:pdf,png,jpeg,jpg|max:2048',
            'id_proof' => 'mimes:pdf,png,jpeg,jpg|max:2048'
        ],[
            'address_proof.mimes' => 'Address Proof image must be of type .pdf, .png, .jpeg, .jpg',
            'address_proof.max' => 'Address Proof image can be maximum 2 MB',
            'id_proof.mimes' => 'ID Proof must be of type .pdf, .png, .jpeg, .jpg',
            'id_proof.max' => 'ID Proof image can be maximum 2 MB',
        ]);

        if ($request->file('id_proof') != '' || $request->file('address_proof') != '') {

            $userInfo = UserDetails::whereUserId(Auth::id())->select('address_proof', 'id_proof')->first();
            $insertData['address_proof'] = isset($userInfo->address_proof) ? $userInfo->address_proof : null;
            $insertData['id_proof'] = isset($userInfo->id_proof) ? $userInfo->id_proof : null;
            $insertData['updated_at'] = \Carbon\Carbon::now();

            if ($request->file('id_proof') && $request->file('id_proof')->isValid()) {

                $image = $request->file('id_proof');
                $extension = $image->getClientOriginalExtension(); // getting image extension;
                $filename = 'id_proof_' . Auth::id() . '_' . date("YmdHis") . rand(1111, 9999) . '.' . $extension;

                $storage_path = 'id_proof/';

                Storage::disk('local')->put($storage_path.$filename.'.aes', Crypt::encrypt(file_get_contents($image->getRealPath())));

                //$file = Storage::get($storage_path.$filename.'.aes');
                //Storage::put($storage_path.$filename,Crypt::decrypt($file));

                $insertData['id_proof'] = $filename;

            }

            if ($request->file('address_proof') && $request->file('address_proof')->isValid()) {

                $image = $request->file('address_proof');
                $extension = $image->getClientOriginalExtension(); // getting image extension;
                $filename = 'address_proof_' . Auth::id() . '_' . date("YmdHis") . rand(1111, 9999) . '.' . $extension;

                $storage_path = 'address_proof/';

                Storage::disk('local')->put($storage_path.$filename.'.aes', Crypt::encrypt(file_get_contents($image->getRealPath())));

                //$file = Storage::get($storage_path.$filename.'.aes');
                //Storage::put($storage_path.$filename,Crypt::decrypt($file));

                $insertData['address_proof'] = $filename;
            }
            if ($userInfo) {
                $check_upload = UserDetails::whereUserId(Auth::id())->update($insertData); // update into user detail
            } else {
                $insertData['created_at'] = \Carbon\Carbon::now();
                $insertData['user_id'] = Auth::id();
                $check_upload = UserDetails::insert($insertData); // insert into user detail
            }

            $request->session()->flash('success_msg', 'Profile Update Successfully.');
            return redirect()->back();

        }


    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\User;
use App\Traits\CaptureIpTrait;
use File;
use Helper;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Image;
use View;

class ProfilesController extends Controller
{

    protected $idMultiKey     = '618423'; //int
    protected $seperationKey  = '****';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function profile_validator(array $data)
    {
        return Validator::make($data, [
            'theme_id'          => '',
            'location'          => '',
            'bio'               => 'max:500',
            'twitter_username'  => 'max:50',
            'github_username'   => 'max:50',
            'avatar'            => '',
            'avatar_status'     => '',
        ]);
    }

    /**
     * Fetch user
     * (You can extract this to repository method)
     *
     * @param $username
     * @return mixed
     */
    public function getUserByUsername($username)
    {
        return User::with('profile')->wherename($username)->firstOrFail();
    }

    public function getUserById($id)
    {
        return User::with('profile')->whereid($id)->firstOrFail();
    }

    public function getUserLikeById($id)
    {
        return count(User::find($id)->userlike);
    }

    public function getUserFollowById($id)
    {
        return count(User::find($id)->userfollow);
    }

    public function getUserConnectionById($id) // Fetch atmost 4 connections of current  user
    {
        $connections = User::find($id)->userconnection;
        $return_array = array();
        if(count($connections) > 0)
        {
            $connected_member_count = 1;
            foreach($connections as $connection)
            {
                if($connected_member_count > 4)
                {
                    break;
                }
                else
                {
                    $return_array[$connection->id] = $this->getUserById($connection->id);
                    $connected_member_count++;
                }
            }
            return $return_array;
        }
        else
        {
            return '0';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param string $username
     * @return Response
     */
    public function show($username)
    {
        try {

            $user = $this->getUserById($username);

        } catch (ModelNotFoundException $exception) {

            abort(404);

        }
      
        $user->member_since = date('F Y',strtotime($user->created_at)); // Chnage Date format of created_at to show Month and Year

        $user->likes = $this->getUserLikeById($user->id); // Fetch number of likes to the current user

        $user->articles = '0'; // Fetch number of articles of current user.

        $user->comments = '0'; // Fetch number of comments of current user.

        $user->following = $this->getUserFollowById($user->id); // Fetch number of followers of current user.

        $user->connections = $this->getUserConnectionById($user->id); // Fetch Connections of current user.

        $data = [
            'user' => $user
        ];



        //return view('profiles.show')->with($data);
        return view('profiles.show2')->with($data);

    }

    /**
     * /profiles/username/edit
     *
     * @param $username
     * @return mixed
     */
    public function edit($username)
    {
        try {

            $user = $this->getUserByUsername($username);

        } catch (ModelNotFoundException $exception) {
            return view('pages.status')
                ->with('error', trans('profile.notYourProfile'))
                ->with('error_title', trans('profile.notYourProfileTitle'));
        }

        $data = [
            'user'          => $user
        ];

        return view('profiles.edit')->with($data);

    }

    /**
     * Update a user's profile
     *
     * @param $username
     * @return mixed
     * @throws Laracasts\Validation\FormValidationException
     */
    public function update($username, Request $request)
    {
        $user = $this->getUserByUsername($username);

        $input = Input::only('theme_id', 'location', 'bio', 'twitter_username', 'github_username', 'avatar_status');

        $profile_validator = $this->profile_validator($request->all());

        if ($profile_validator->fails()) {

            $this->throwValidationException(
                $request, $profile_validator
            );

            return redirect('profile/'.$user->name.'/edit')->withErrors($validator)->withInput();
        }

        if ($user->profile == null) {

            $profile = new Profile;
            $profile->fill($input);
            $user->profile()->save($profile);

        } else {

            $user->profile->fill($input)->save();

        }

        $user->updated_ip_address = $this->getClientIp();

        $user->save();

        return redirect('profile/'.$user->name.'/edit')->with('success', trans('profile.updateSuccess'));

    }

    /**
     * Get a validator for an incoming update user request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'name'              => 'required|max:255',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateUserAccount(Request $request, $id)
    {

        $currentUser = \Auth::user();
        $user        = User::findOrFail($id);
        $emailCheck  = ($request->input('email') != '') && ($request->input('email') != $user->email);

        $validator = Validator::make($request->all(), [
            'name'      => 'required|max:255',
        ]);

        $rules = [];

        if ($emailCheck) {
            $rules = [
                'email'     => 'email|max:255|unique:users'
            ];
        }

        $validator = $this->validator($request->all(), $rules);

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user->name         = $request->input('name');
        $user->first_name   = $request->input('first_name');
        $user->last_name    = $request->input('last_name');

        if ($emailCheck) {
            $user->email = $request->input('email');
        }

        $user->updated_ip_address = $this->getClientIp();

        $user->save();

        return redirect('profile/'.$user->name.'/edit')->with('success', trans('profile.updateAccountSuccess'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateUserPassword(Request $request, $id)
    {

        $currentUser = \Auth::user();
        $user        = User::findOrFail($id);

        $validator = Validator::make($request->all(),
            [
                'password'              => 'required|min:6|max:20|confirmed',
                'password_confirmation' => 'required|same:password',
            ],
            [
                'password.required'     => trans('auth.passwordRequired'),
                'password.min'          => trans('auth.PasswordMin'),
                'password.max'          => trans('auth.PasswordMax'),
            ]
        );

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        if ($request->input('password') != null) {
            $user->password = bcrypt($request->input('password'));
        }

        $user->updated_ip_address = $this->getClientIp();

        $user->save();

        return redirect('profile/'.$user->name.'/edit')->with('success', trans('profile.updatePWSuccess'));

    }

    /**
     * Upload and Update user avatar
     *
     * @param $file
     * @return mixed
     */
    public function upload() {
        if(Input::hasFile('file')) {

            $currentUser  = \Auth::user();
            $avatar       = Input::file('file');
            $filename     = 'avatar.' . $avatar->getClientOriginalExtension();
            $save_path    = storage_path() . '/users/id/' . $currentUser->id . '/uploads/images/avatar/';
            $path         = $save_path . $filename;
            $public_path  = '/images/profile/' . $currentUser->id . '/avatar/' . $filename;

            // Make the user a folder and set permissions
            File::makeDirectory($save_path, $mode = 0755, true, true);

            // Save the file to the server
            Image::make($avatar)->resize(300, 300)->save($save_path . $filename);

            // Save the public image path
            $currentUser->profile->avatar = $public_path;
            $currentUser->profile->save();

            return response()->json(array('path'=> $path), 200);

        } else {

            return response()->json(false, 200);

        }
    }

    /**
     * Show user avatar
     *
     * @param $id
     * @param $image
     * @return string
     */
    public function userProfileAvatar($id, $image)
    {
        return Image::make(storage_path() . '/users/id/' . $id . '/uploads/images/avatar/' . $image)->response();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteUserAccount(Request $request, $id)
    {

        $currentUser = \Auth::user();
        $user        = User::findOrFail($id);

        $validator = Validator::make($request->all(),
            [
                'checkConfirmDelete'            => 'required',
            ],
            [
                'checkConfirmDelete.required'   => trans('profile.confirmDeleteRequired'),
            ]
        );

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        if ($user->id != $currentUser->id) {

            return redirect('profile/'.$user->name.'/edit')->with('error', trans('profile.errorDeleteNotYour'));

        }

        // Create and encrypt user account restore token
        $sepKey       = $this->getSeperationKey();
        $userIdKey    = $this->getIdMultiKey();
        $restoreKey   = config('settings.restoreKey');
        $encrypter    = config('settings.restoreUserEncType');
        $level1       = $user->id * $userIdKey;
        $level2       = urlencode(Uuid::generate(4) . $sepKey . $level1);
        $level3       = base64_encode($level2);
        $level4       = openssl_encrypt($level3, $encrypter, $restoreKey);
        $level5       = base64_encode($level4);

        // Save Restore Token and Ip Address
        $user->token  = $level5;
        $user->deleted_ip_address = $this->getClientIp();
        $user->save();

        // Send Goodbye email notification
        $this->sendGoodbyEmail($user, $user->token);

        // Soft Delete User
        $user->delete();

        // Clear out the session
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect('/login/')->with('success', trans('profile.successUserAccountDeleted'));

    }

    /**
     * Send GoodBye Email Function via Notify
     *
     * @param array $user
     * @param string $token
     * @return void
     */
    public static function sendGoodbyEmail(User $user, $token) {
        $user->notify(new SendGoodbyeEmail($token));
    }

    /**
     * Get User Restore ID Multiplication Key
     *
     * @return string
     */
    public function getIdMultiKey() {
        return $this->idMultiKey;
    }

    /**
     * Get User Restore Seperation Key
     *
     * @return string
     */
    public function getSeperationKey() {
        return $this->seperationKey;
    }

}

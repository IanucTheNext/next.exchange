<?php

namespace App\Http\Controllers;
use Event;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Coin;
use App\Models\Wallet;
use App\Events\Transactionupdate;
use App\Services\Blockchainservice;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $content = [];

	    $id = Auth::id();

        $data   = Transaction::where('transaction_user_id', $id )
        ->orderBy('updated_at', 'desc')
        ->paginate(25);

	foreach($data as $alldata)
	{

		$market         = $alldata->market_name;
		$cointitle      = $alldata->maincoin_name;
		$qunatitycoin   = $alldata->transaction_maincoin_amount;


		$coindate=date('d M y H:m ', strtotime($alldata->created_at));
		$content[]=array(
		    'transaction_date'      => $coindate,
            'transaction_type'      => $alldata->transaction_type,
            'transaction_buysell'   => $alldata->transaction_buysell,
            'gas_fee_amount'        => $alldata->gas_fee_amount,
            'amount'                => $qunatitycoin,
            'main_coin'             => $cointitle,
            'status'                => $alldata->transaction_status
		);
	}
		$response = [
				'pagination' => [
					'total' => $data->total(),
					'per_page' => $data->perPage(),
					'current_page' => $data->currentPage(),
					'last_page' => $data->lastPage(),
					'from' => $data->firstItem(),
					'to' => $data->lastItem()
				],
				'data' => $content
				];

	 //event(new Transactionupdate($data));
     return response()->json($response);
}


	public function userdisclaimer(Request $request)
	{

		$id = Auth::user()->id;
		$user=User::where('user_disclaimer',1)->where('id', $id)->first();
		if($user)
		{
			$success=1;
		}
		else{
			$success=0;
		}
		$data=array('success'=>$success);
		return response()->json(['data'=>$data]);

	}



    public function saveuserdisclaimer(Request $request)
	{

		$id = Auth::user()->id;
		$user=User::find($id);

		$user->user_disclaimer = 1;
		$user->save();


		return response()->json(1);
	}


    // TODO: new private function to insert transactions in the table
    // We have 3 types of transactions.
    // Incoming = people deposit money
    // Internal = an exchange has happen
    // Deposit = people withdraw money
    public function saveTransaction() {

        $transaction = new Transaction();
        $id = Auth::id();
        $transaction->transaction_user_id = $id;
        $transaction->transaction_txid = $response["TRANSACTIONID"];
        $transaction->transaction_rxid = str_random(60);
        $transaction->transaction_addr = 'paypal';
        $transaction->transaction_amount = $request->get('amount');
        $transaction->transaction_market = 1;
        $transaction->transaction_fee = 000000000000000000;
        $transaction->transaction_cost = 000000000000000000;
        $transaction->transaction_ip = $request->ip();
        $transaction->transaction_price = 0;
        $transaction->transaction_buysell = 0; // 0 = buy, 1 = sell
        $transaction->transaction_maincoin = $crt;
        $transaction->transaction_maincoin_wallet_id = '1';
        $transaction->transaction_maincoin_amount = 100;
        $transaction->transaction_maincoin_wallet_balance = '0';
        $transaction->transaction_confirmations = 0;
        $transaction->transaction_status = 1;
        $transaction->created_at = \Carbon\Carbon::now();
        $transaction->updated_at = \Carbon\Carbon::now();
        $transaction->save();

        // Here comes the execution of Crypto
        // So blockchain exchange here like:
        // cryptoswap(ETH, 1000, $buyerID, NEXT, 10, $sellerID);
    }

    // TODO: getting more details about the transaction
    // This function will get the transactionID and date of a specific user id
    public function getTransactions($user_id) {

    }

    // Fetch a list of tranasaction from blockchain service and save it to db.
    public function saveTransactions(Request $request, $symbol, $address) {
        $blockChain = new Blockchainservice;
        $response  = $blockChain->getTransactionList( $symbol,$address );

        if ($response['status'] == true) {
                $token = Coin::getNameByID($symbol);
                foreach ($response['data'] as $deposit) {
                    $wallet = Wallet::where('tx_id' , $deposit->get('txid'))->first();
                    $tx = Transaction::where('transaction_txid', $deposit->get('txid'))->first();
                    if (!(property_exists($tx, 'transaction_id') && $tx->transaction_id != NULL)) {
                        $transaction = new Transaction();
                        $id = Auth::id();
                        $transaction->transaction_user_id = $id;
                        $transaction->transaction_txid = $deposit->get('txid');
                        $transaction->transaction_rxid = str_random(60);
                        $transaction->transaction_amount = $deposit->get('amount');
                        $transaction->transaction_ip = $deposit->ip();
                        $transaction->transaction_type = $deposit->get('type');
                        $transaction->transaction_maincoin = $token->coin_id;
                        $transaction->transaction_maincoin_wallet_id = $wallet->wallet_id;
                        $transaction->created_at = \Carbon\Carbon::createFromTimestamp($deposit->get('time'));
                        $transaction->updated_at = \Carbon\Carbon::createFromTimestamp($deposit->get('time'));
                        $transaction->save();
                    }                
                }
        }
        return response()->json(['status' => true]);
    }
}

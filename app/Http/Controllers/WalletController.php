<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\Addresses;
use App\Models\Coin;
use App\Models\Transaction;
use App\Traits\CaptureIpTrait;
use Illuminate\Http\Request;
use App\Models\Wallet;
use App\Models\AddressesGenerate;
use Carbon\Carbon;
use App\Services\Blockchainservice;
use GenPhrase;
use Illuminate\Support\Facades\Auth;


class WalletController extends Controller
{
    use CaptureIpTrait;

     /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function initWallet()
    {
        $user   = Auth::user();

        /**
         * if all default wallets are not created creating it
         */
        if($user->wallet_added == 0) {

            $default_wallets    = [1, 2, 12, 9, 10, 11];

            /**
             * Checking any of the default wallets created already.
             *
             * Some cases because of node issues wallets may not be created
             * To handle this situation this checking is added
             */
            $walletCreated      = Wallet::where('user_id', $user->id)
                ->whereIn('coin_id', $default_wallets)
                ->get();

            $wallet_created = [];
            foreach($walletCreated as $created) {

                $wallet_created[] = $created->coin_id;
            }

            $to_create      = array_diff($default_wallets, $wallet_created);

            $toCreateCount  = count($to_create);
            $create_index   = 0;

            //Generating the address and wallets
            foreach ($to_create as $coin_id) {

                //If not fiat coins
                if(! in_array($coin_id, [10, 11])) {

                    $address    = Addresses::generateAddressByUserID( $user->id, $coin_id );

                    if(!is_null( $address )) $create_index++;
                } else {

                    $wallet     = new Wallet;

                    $wallet->tx_id          = Helper::uniqueID();
                    $wallet->user_id        = $user->id;
                    $wallet->coin_id        = $coin_id;
                    $wallet->name           = Coin::getNameByID( $coin_id );
                    $wallet->amount         = 0.0;
                    $wallet->save();

                    unset($wallet);

                    $create_index++;
                }
            }

            //Checking all defaults wallets are created or not, if yes updating the user table field
            if($toCreateCount === $create_index) {

                $user->wallet_added = 1;
                $user->save();
            }
        }

        //Getting data from blockchain and wallets
        //Get wallet with address and return to vue
        $enabled_wallets    = Wallet::getWalletWithAddress( $user->id );
        return response()->json($enabled_wallets);
    }

    public function updateExpiredAddresses($iCoinMarket) {
        $expired_date = Carbon::now()->subMinutes(30)->toDateTimeString();
        $result = AddressesGenerate::join('transactions', 'addresses_generate.address', '=', 'transactions.transaction_addr')
            ->where('addresses_generate.coin_market','=', $iCoinMarket)
            ->where('addresses_generate.expired','=',0)
            ->where('addresses_generate.updated_at','<',$expired_date)
            ->where('transactions.transaction_status','=',0)
            ->get();

        if($result->count()) {
            foreach($result as $item) {
                // Update status of expired
                AddressesGenerate::where('address', $item->address)->update(['expired' => 1]);
            }
        }
    }

    public function getAddrBitcoin(Request $request, $txid = '') { // Depriciated
        $bitcoin = new BitcoinController();
        $oBitcoin = $bitcoin->getBitcoinAddress();

        $btc_price = \App\Helpers\Helper::getCryptoPrice('','BTC');
        $eth_price = \App\Helpers\Helper::getCryptoPrice('','ETH');

        $next_amount = $request->amount;
        $next_price = $next_amount * ($eth_price / 1000);

        $next_btc_amount = $next_price / $btc_price;

        if (empty($txid)) {
            $txid = str_random(64);
        }

        $transaction = new Transaction();
        $transaction->transaction_user_id = Auth::id();
        $transaction->transaction_txid = $oBitcoin->txid;
        $transaction->transaction_addr = $oBitcoin->address;
        $transaction->transaction_amount = $next_btc_amount;
        $transaction->transaction_market = 1; // BTC
        $transaction->transaction_ip = self::getClientIp();
        $transaction->transaction_price = '0';
        $transaction->transaction_buysell = '0'; // 0 = buy, 1 = sell
        $transaction->transaction_maincoin_amount = $next_amount;
        $transaction->transaction_maincoin = '12';
        $transaction->save();

        $html = '
<style>
#qr-code { text-align: center !important; }
</style>
        <div class="card ptb40 col-12" style="min-height: 260px">

<div class="row">
                    <div class="col-3">
                    <script src="components/jquery-qrcode/dist/jquery-qrcode.js"></script>
                
                    <div id="qrcode"><a href="bitcoin:'.$oBitcoin->address.'?amount='.$next_btc_amount.'"></a></div>
                    <script>
                        $(function() {
                            $(\'#qrcode a\').qrcode({
                                render: \'div\',
                                text: "bitcoin:'.$oBitcoin->address.'?amount='.$next_btc_amount.'",
                                ecLevel: \'L\',
                                size: "120"
                            });
                        });
                    </script>
                    </div>
                    <div class="col-9" style="text-align: left">
                     <br>Send manual <b>'.$next_btc_amount.' BTC</b> to:<br> <span class="f16"><b>'.$oBitcoin->address.'</b></span>
                     </div>
                     </div>
                     <div class="col-12">
                    <br><small>Payment is only possible within 15 minutes.</small>
                    <br> After completing the payment, wait for <b>3</b> confirmations</a>. <br>After confirmation we will add <b>'.$next_amount.' NEXT</b> tokens to your wallet.
                    </div>

                </div>
        ';

        return $html;
    }

    public static function genPassphrase() {
        $gen = new GenPhrase\Password();
        $gen->disableSeparators(true);
        $gen->disableWordModifier(true);
        return $gen->generate(120);
    }

    /**
     * Fetch the coins
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCoins()
    {
        $coins  = Coin::select('coin_id', 'coin_coin AS symbol')
            ->where('coin_enabled', 1)
            ->whereNotIn('coin_id', [1, 2, 10, 11, 12])
            ->get();

        return response()->json($coins);
    }

    /**
     * Show the selected coin
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showCoin(Request $request)
    {
        $coin_id    = $request->input('coin_id', 0);
        $wallet = Wallet::where('coin_id', $coin_id)
            ->where('user_id', Auth::id())
            ->first();

        $wallet_data    = [
            'wallet_id'         => 0,
            'wallet_enabled'    => 0
        ];

        if ($wallet) {

            $wallet_data    = [
                'wallet_id'         => $wallet->id,
                'wallet_enabled'    => $wallet->wallet_enabled
            ];
        }

        return response()->json($wallet_data);
    }

    public function createUpdateWallet(Request $request)
    {
        $wallet_id      = $request->input('wallet_id', 0);
        $coin_id        = $request->input('coin_id');
        $wallet_enabled = $request->input('wallet_enabled', 0);
        $user_id        = Auth::id();
        $wallet         = null;

        if($wallet_id == 0 && $wallet_enabled == 1) {

            Addresses::generateAddressByUserID( $user_id, $coin_id );

            $wallet     = Wallet::where('user_id', $user_id)
                ->where('coin_id', $coin_id)
                ->first();

        } elseif ($wallet_id != 0) {

            $wallet                 = Wallet::find($wallet_id);
            $wallet->wallet_enabled = $wallet_enabled;
            $wallet->save();
        }

        return $wallet ? response()->json([
            'wallet_id'         => $wallet->id,
            'wallet_enabled'    => $wallet_enabled
        ]) : response()->json([]);
    }
}

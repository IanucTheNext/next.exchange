<?php

namespace App\Http\Middleware;

use App\Support\Google2FAAuthentication;
use Closure;

class Google2FAMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authentication = app(Google2FAAuthentication::class)->boot($request);

        // Show Google 2FA if not authenticated with Google
        if ($authentication->isAuthenticated()) {
            return $next($request);
        }

        // Otherwise return oneTimePasswordResponse
        // TODO: If Google2FA is not enabled!

        //return $authentication->makeRequestOneTimePasswordResponse();
        return $next($request);

    }
}


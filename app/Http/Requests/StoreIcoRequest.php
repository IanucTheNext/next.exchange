<?php
/*
File created to validate Contact form before submitting.
*/

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreIcoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|regex:/(^([a-zA-Z ]+)(\d+)?$)/u',
            'symbol' => 'required|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'total_supply_token' => 'required|numeric',
            'stage' => 'required|regex:/(^([a-zA-Z ]+)(\d+)?$)/u',
            'launch_date' => 'required|date',
            'initial_price' => 'required|numeric',
            'short_description' => 'required|regex:/(^([a-zA-Z0-9 ]+)(\d+)?$)/u',
            'full_description' => 'required|regex:/(^([a-zA-Z0-9 ]+)(\d+)?$)/u'
        ];
    }
}
